<?php
/*
Template Name: Политика конфиденциальности
Template Post Type: page
*/
?>
<?php get_header(); ?>


<!-- #MainSlider -->
<div id="MainSlider">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 offset-sm-1 order-sm-last">
                <h1 class="main-slider-title ">Квартиры в Пензе от застройщиков</h1>
                <div class="row">
                    <div class="col-sm-6 pr">Все строительные компании: каталог жилой недвижимости с ценами</div>
                    <div class="col-sm-6 pr">20 жилых комплексов: подбор квартир бесплатно</div>
                </div>
            </div>
            <div class="col-sm-5 ">
                <div class="main-slider-intro ">
                    <div class="btn btn-intro">
                        <div class="btn-intro--text">
                            <div class="main-slider-intro--title">Выгодная ипотека от 4,5%</div>
                            Найдем самую выгодную процентную ставку<br>
                            по ипотеке среди всех банков.
                        </div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="12">
                            <path d="M17.072 0l-1.048 1.048 4.663 4.663H0v1.482h20.687l-4.663 4.663 1.048 1.048 6.452-6.452z" fill="#fff" />
                        </svg>
                    </div>
                    <div class="main-slider-intro-g"></div>

                </div>
                <a class="main-slider-phone" href="tel:<?= get_option('themex_info__tel') ?>"><?= get_option('themex_info__tel') ?></a>
                <a class="main-slider-call" href="#" data-toggle="modal" data-target="#za">Заказать звонок</a>
            </div>
        </div>
    </div>
</div>
<div class="section section-content">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1><?= get_the_title() ?></h1>
                <hr>
            </div>

            <div class="col-12">
                <div>
                    <?= get_the_content(); ?>
                </div>

            </div>
        </div>
    </div>
</div>



<!-- #FormSelect -->

<?php include "templates/section-form.php" ?>


<!-- #Contact -->

<?php include "templates/section-contact.php" ?>




<?php get_footer(); ?>