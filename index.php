<?php
/*
Template Name: Главная страница
Template Post Type: page
*/



function enqueueScriptsMain()
{
    wp_enqueue_script('js-main', get_template_directory_uri() . "/assets/js/script-main.js", array('jquery'), 1.0, true);
}
add_action('wp_enqueue_scripts', 'enqueueScriptsMain');
?>
<?php get_header(); ?>
<div class="section section-main-slider">
    <?= ThemexInterface::renderSliderMain(get_field("section_title")) ?>
</div>
<?php
/*
<div id="MainSlider">
    <div class="container-fliud h-100">
        <div class="row h-100">
            <?= ThemexInterface::renderSliderMain(get_field("section_title")) ?>
        </div>
    </div>
</div>
*/
?>
<?= ThemexInterface::renderSectionPlus(get_field("section_plus")) ?>
<?php include "templates/section-complexes.php" ?>
<?= ThemexInterface::renderSectionSeparation(get_field("section_separator")) ?>
<?= ThemexInterface::renderSectionHelp(get_field("section_help")) ?>
<?= ThemexInterface::renderSectionAbout(get_field("section_about")) ?>
<?= ThemexInterface::renderSectionServices(get_field("section_services")) ?>
<?php include "templates/section-builders.php" ?>
<?= ThemexInterface::renderSectionDiff(get_field("section_diff")) ?>
<?= ThemexInterface::renderSectionInstagram(get_field("section_instagramm")) ?>
<?= ThemexInterface::renderSectionFree(get_field("section_free")) ?>
<?= ThemexInterface::renderSectionFormRequest() ?>
<?php include "templates/section-contact.php" ?>
<?php get_footer(); ?>