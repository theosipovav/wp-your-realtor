
var duration = 200;
var easing = 'ease-out';
var perPage = 1;
var startIndex = 0;
var draggable = true;
var multipleDrag = true
var threshold = 20;
var loop = false;
var rtl = false;
var count = 10;

var siema = new Array(10);

for (let i = 0; i < 10; i++) {
    var selector = '.slider-structures-' + i;
    if (document.querySelector(selector) != null) {
        siema[i] = new Siema({
            selector: selector,
            duration: duration,
            easing: easing,
            perPage: perPage,
            startIndex: startIndex,
            draggable: draggable,
            multipleDrag: multipleDrag,
            threshold: threshold,
            loop: loop,
            rtl: rtl,
            onInit: () => { },
            onChange: () => { },
        });
        document.querySelector('.slider-structures--prev-' + i).addEventListener('click', () => siema[i].prev());
        document.querySelector('.slider-structures--next-' + i).addEventListener('click', () => siema[i].next());

    }
}


jQuery('.lightzoom').lightzoom({
    speed: 400,   // скорость появления
    imgPadding: 10,    // значение отступа у изображения
    overlayOpacity: '0.5', // прозрачность фона (от 0 до 1)
    viewTitle: false, // true, если надо показывать подпись к изобажению
    isOverlayClickClosing: true, // true, если надо закрывать окно при клике по затемненной области
    isWindowClickClosing: false, // true, если надо закрывать окно при клике по любой области
    isEscClosing: true, // true, если надо закрывать окно при нажатии на кнопку Esc
    boxClass: '',    // позволяет задавать класс окну обертке (с версии 1.1.0)
    overlayColor: '',    // позволяет задавать цвет фону (с версии 1.1.0)
    titleColor: '',    // позволяет задавать цвет подписи (с версии 1.1.0)
});