


jQuery(document).ready(function ($) {

    /**
     * Инициализация слайдера Siema для застройщиков на главное странице
     */
    var sliderZasCat = null;
    if (document.querySelector('.slider-zascat') != null) {
        sliderZasCat = new Siema({
            selector: '.slider-zascat',
            duration: 200,
            easing: 'ease-out',
            perPage: {
                768: 1,
                1024: 2,
            },
            startIndex: 1,
            draggable: true,
            multipleDrag: true,
            threshold: 20,
            loop: true,
            rtl: false,
            onInit: () => { },
            onChange: () => { },
        });
        const sliderZasCatPrev = document.querySelector('.slider-zascat-container .slider--prev');
        const sliderZasCatNext = document.querySelector('.slider-zascat-container .slider--next');
        sliderZasCatPrev.addEventListener('click', () => sliderZasCat.prev());
        sliderZasCatNext.addEventListener('click', () => sliderZasCat.next());
        setInterval(() => sliderZasCat.next(), 5000)
    }


    var sliderComplexes = null;
    if (document.querySelector('.slider-complexes') != null) {
        sliderComplexes = new Siema({
            selector: '.slider-complexes',
            duration: 200,
            easing: 'ease-out',
            perPage: {
                0: 1,
            },
            startIndex: 1,
            draggable: true,
            multipleDrag: true,
            threshold: 20,
            loop: true,
            rtl: false,
            onInit: () => { },
            onChange: () => { },
        });
        const sliderComplexesPrev = document.querySelector('.slider-complexes-container .slider--prev');
        const sliderComplexesNext = document.querySelector('.slider-complexes-container .slider--next');
        sliderComplexesPrev.addEventListener('click', () => sliderComplexes.prev());
        sliderComplexesNext.addEventListener('click', () => sliderComplexes.next());
        //setInterval(() => sliderComplexes.next(), 5000)
    }





    const sliderMain = $(".slider-main");
    var sliderContainer = sliderMain.find(".slider-main-container");
    var slides = sliderMain.find(".slider-main-slide");
    var activeSlide = 0;
    var countSlides = slides.length;



    setInterval(() => {
        if (activeSlide >= countSlides - 1) {
            activeSlide = 0;
        } else {
            activeSlide++;
        }
        $(slides).removeClass("active");
        $(slides[activeSlide]).addClass("active");
        let value = 100 * activeSlide;
        let valueString = "translateX(-" + value + "vw)";
        $(sliderContainer).css("transform", valueString);
    }, 7000);

});