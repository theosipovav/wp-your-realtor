
jQuery(function ($) {





    var companyInfo = $('.table-layouts-schemes');

    // Добавить схему планировки
    $('.list-admin-layouts--add', companyInfo).click(function (e) {
        e.preventDefault();
        var table = $('.table-layouts-schemes');
        row = table.find('.table-layouts-schemes-row').first().clone();
        row.find('input').val('');
        var src = row.find('.list-admin-layouts-img img').attr('data-src');
        row.find('.list-admin-layouts-img img').attr('src', src);
        row.find('.list-admin-layouts-img input').val('');
        table.append(row);
    });


    // Сформировать автоматически из выбранных
    $('.list-admin-layouts--add-multi', companyInfo).click(function (e) {
        e.preventDefault();
        wp.media.editor.send.attachment = function (props, attachment) {
            var url = attachment.url;
            var title = attachment.title;
            var alt = attachment.alt;
            var table = $('.table-layouts-schemes');
            row = table.find('.table-layouts-schemes-row').first().clone();
            row.find('.list-admin-layouts-img img').attr('src', url);
            row.find('.list-admin-layouts-img img').attr('title', title);
            row.find('.list-admin-layouts-img img').attr('alt', alt);
            row.find('input.list-admin-layouts-img--id').val(attachment.id);
            if (title != "") {
                row.find('input.list-admin-layouts-input').val(title);
            }
            if (alt != "") {
                row.find('input.list-admin-layouts-input').val(alt);
            }
            table.append(row);
        }
        wp.media.editor.open();
        return false;
    });



    /**
     * Действие при нажатии на кнопку загрузки изображения
     * вы также можете привязать это действие к клику по самому изображению
     */
    companyInfo.on('click', '.button-upload-image', function () {
        var send_attachment_bkp = wp.media.editor.send.attachment;


        var button = jQuery(this);
        wp.media.editor.send.attachment = function (props, attachment) {

            var url = attachment.url;
            var title = attachment.title;
            var alt = attachment.alt;
            jQuery(button).parent().prev().attr('src', url);
            jQuery(button).parent().prev().attr('alt', alt);
            jQuery(button).parent().prev().attr('title', title);
            jQuery(button).prev().val(attachment.id);
            var input = jQuery(button).closest('.table-layouts-schemes-row').find('input.list-admin-layouts-input');
            if (jQuery(input).val() == "") {
                if (title != "") {
                    jQuery(input).val(title);
                }
                if (alt != "") {
                    jQuery(input).val(alt);
                }
            }
            wp.media.editor.send.attachment = send_attachment_bkp;
        }
        wp.media.editor.open(button);
        return false;
    });


    /**
     * удаляем значение произвольного поля
     * если быть точным, то мы просто удаляем value у input type="hidden"
     */
    companyInfo.on('click', '.button-remove-image', function () {
        var src = jQuery(this).parent().prev().attr('data-src');
        jQuery(this).parent().prev().attr('src', src);
        jQuery(this).prev().prev().val('');
        return false;
    });


    // Удаляет бокс 
    companyInfo.on('click', '.button-remove', function () {
        if ($('.table-layouts-schemes-row').length > 1) {
            $(this).closest('.table-layouts-schemes-row').remove();
        } else {
            var src = $(this).closest('.table-layouts-schemes-row .list-admin-layouts-img img').attr('data-src');
            $(this).closest('.table-layouts-schemes-row .list-admin-layouts-img img').attr('src', src);
            $(this).closest('.table-layouts-schemes-row').find('.list-admin-layouts-img input').val('');
            $(this).closest('.table-layouts-schemes-row').find('.list-admin-layouts-input').val('');
        }
    });




});



