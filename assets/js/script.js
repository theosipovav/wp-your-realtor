




/**
 * 
 */
jQuery.noConflict();
jQuery(document).ready(function ($) {
    /**
     * 
     */
    $(".tab-nav-link").click(function (e) {
        e.preventDefault();
        var idPane = $(this).attr("href");
        var pane = $(idPane);
        $(".tab-nav-link").removeClass("active");
        $(this).addClass("active");
        $(".tab-pane").removeClass("active");
        $(".tab-pane").removeClass("show");
        pane.addClass("active");
        pane.addClass("show");
    });
    /**
     * 
     */
    $(".btn-modal-open").click(function (e) {
        e.preventDefault();
        var idModal = $(this).attr("data-modal");
        var modal = $(idModal);
        $(modal).addClass("show");
        $(modal).addClass("active");
    });
    /**
     * 
     */
    $(".btn-modal-close").click(function (e) {
        e.preventDefault();
        var idModal = $(this).attr("data-modal");
        var modal = $(idModal);
        $(modal).removeClass("show");
        $(modal).removeClass("active");
    });

    $(".modal").click(function (e) {

        var div = $(".modal-dialog");
        if (!div.is(e.target) && div.has(e.target).length === 0) {
            if ($(this).hasClass("show") == true && $(this).hasClass("active") == true) {
                $(this).removeClass("show");
                $(this).removeClass("active");
            }
        }



    });


    /**
     * Открыть модальное окно
     */
    $(".nav-mobile-close").click(function (e) {
        e.preventDefault();
        $(".nav-mobile-container").removeClass("active");
        $("body").removeClass("body-modal");

    });

    /**
     * Закрыть модальное окно
     */
    $(".nav-mobile-open").click(function (e) {
        e.preventDefault();
        $(".nav-mobile-container").addClass("active");
        $("body").addClass("body-modal");
    });



    /**
     * Создание маски ввода для телефонного номера
     */
    var inputTel = $("input.form-control-tel");
    inputTel.inputmask({
        mask: "+7 (999) 999-99-99",
        showMaskOnHover: true,
    });

    /**
     * Создание маски ввода для телефонного номера
     */
    var inputEmail = $("input.form-control-email");
    inputEmail.inputmask({
        mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
        showMaskOnHover: false,
    });




    /**
     * Отправка формы по средствам AJAX запроса
     */
    $(".form-ajax").on('submit', function (e) {
        e.preventDefault();
        var form = this;
        var action = $(form).attr("action");
        $.ajax({
            url: action,
            type: "POST",
            data: $(form).serialize(),
            dataType: "json",
            success: function (res) {
                if (res.status == "success") {
                    $(form).removeClass("form-error");
                    $(form).addClass("form-success");
                    $(form).find(".form-status--text").html('Ваша заявка отправлена!');
                    window.setTimeout(function () {
                        $(form).removeClass("form-error");
                        $(form).removeClass("form-success");
                        $(form).find(".form-status--text").html('');
                        $(form).trigger("reset");
                    }, 3000);
                }
            },
            error: function (a, b, c) {
                $(form).removeClass("form-success");
                $(form).addClass("form-error");
                $(form).find(".form-status--text").html('Не удалось отправить заявку<br>Пожалуйста, повторите попытку позже.');
                console.log(a);
                console.log(b);
                console.log(c);
                window.setTimeout(function () {
                    $(form).removeClass("form-error");
                    $(form).removeClass("form-success");
                    $(form).find(".form-status--text").html('');
                    $(form).trigger("reset");
                }, 3000);
            },
        });

        return false;
    });



    /**
     * Плавная прокрутка к якорю
     */
    $("a.scroll-to").on("click", function (e) {
        var anchor = $(this).attr('href');
        if (anchor.length > 0) {
            console.log(anchor);
            if (anchor[0] == '/') {
                anchor = anchor.substring(1);
            }
            let target = $(anchor);
            if (target.length != 0) {
                e.preventDefault();
                $('html, body').stop().animate({
                    scrollTop: target.offset().top - 60
                }, 800);
            }
        }
    });





    /**
     * Плавающая кнопка
     * Появление
     */
    const floatingCallback = $("#FloatingCallback");
    window.setTimeout(function () {
        floatingCallback.removeClass("fade");
    }, 300);
    const floatingWhatsapp = $("#FloatingWhatsapp");
    window.setTimeout(function () {
        floatingWhatsapp.removeClass("fade");
    }, 350);


    $(window).scroll(function () {
        var height = $(window).scrollTop();
        var elHeader = $(".header-fixed").innerHeight();
        if (height > elHeader * 2) {
            $('.header').addClass('header-mini');
        } else {
            $('.header').removeClass('header-mini');
        }
    });
});




//
//
//
