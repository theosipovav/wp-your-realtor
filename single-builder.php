<?php
/*
Template Name: Застройщик
Template Post Type: builder
*/


// Наименование жилого комплекса
$nameBuilder = get_the_title(get_the_ID());
if ($nameBuilder == null || $nameBuilder == "") {
    $nameBuilder = "Наименование";
}



$args = array(
    'nopaging'               => true,
    'post_type'              => 'nav_menu_item',
    'post_status'            => 'publish',
    'order'                  => 'ASC',
    'orderby'                => 'menu_order',
    'output'                 => ARRAY_A,
    'output_key'             => 'menu_order',
    'update_post_term_cache' => false
);
$itemsMenuBuilder = wp_get_nav_menu_items('Меню для застройщика', $args = array());


?>

<?php get_header(); ?>
<div id="HeaderBuilder" class="section section-header-post">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-sm-3 d-flex justify-content-center justify-content-md-start align-items-md-center">
                <a href="/" class="d-flex justify-content-start"><?= get_the_post_thumbnail(get_the_ID(), 'post-thumbnail', ['class' => 'builder-logo']) ?></a>
            </div>
            <div class="col-sm-7 laptop">
                <ul class="nav">
                    <?php foreach ($itemsMenuBuilder as $key => $item) : ?>
                        <li class="nav-item">
                            <a href="<?= $item->url ?>" class="nav-link"><?= $item->title ?></a>
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>
            <div class="col-sm-2 laptop">
                <a href="#" data-modal="#ModalMain" class="btn-modal-open btn-link-primary-v2">
                    <?= ThemexInterface::renderText('Оставить заявку'); ?>

                </a>
            </div>
        </div>
    </div>
</div>


<div class="section section-banner" style="background-image: url(<?= get_field("picture_bg")['url'] ?>);">
    <div class="container">
        <div class="color-white">Строительная компания</div>
        <h1 class="color-white"><?= $nameBuilder ?></h1>
    </div>

</div>







<div id="AboutBuilder" class="section my-5 p-0">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="h2 py-2">О застройщике</h2>
                <div class="post-content pb-2">
                    <?= wp_strip_all_tags(get_the_content()) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="Map" class="section my-5 p-0 background-form">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 d-flex justify-content-center align-items-center p-0" style="min-height: 390px;">
                <?= get_field("map_code") ?>
            </div>
        </div>
    </div>
</div>

<?php
$complexesAll = get_posts(array(
    'numberposts' => -1,
    'category'    => 0,
    'orderby'     => 'date',
    'order'       => 'DESC',
    'include'     => array(),
    'exclude'     => array(),
    'meta_key'    => '',
    'meta_value'  => '',
    'post_type'   => 'complex',
    'suppress_filters' => true,
));
$complexes = array();
foreach ($complexesAll as $key => $complex) {
    $builderComplexes =  get_field("builder", $complex->ID);
    if ($builderComplexes != null && $builderComplexes->ID == get_the_ID()) {
        array_push($complexes, $complex);
    }
}

?>
<?php if (count($complexes) > 0) : ?>
    <div id="Complexes" class="section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2>Каталог жилых комплексов</h2>
                </div>
            </div>
            <?= ThemexInterface::renderGridComplexes($complexes) ?>
        </div>
    </div>
<?php endif ?>
<?php /* ThemexInterface::renderSectionYouPrice(); */ ?>
<!-- #Diff -->
<div id="Diff " class="section section-diff">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Бесплатные услуги</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="card-diff">
                    <div class="row">
                        <div class="col-sm-2">
                            <img class="card-diff--img" src="<?= get_template_directory_uri() ?>/assets/img/file.svg" alt="" width="64">
                        </div>
                        <div class="col-sm-10">
                            <h4 class="card-diff--title">Подбор квартиры</h4>
                            <p class="card-diff--desc">
                                Не важно, какое жилье вы ищете: однушку, трешку или пентхаус. Менеджер выслушает и составит каталог подходящих квартир У ЗАСТРОЙЩИКА. Выбирайте, смотрите, не торопитесь!
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card-diff">
                    <div class="row">
                        <div class="col-sm-2">
                            <img class="card-diff--img" src="<?= get_template_directory_uri() ?>/assets/img/skills.svg" alt="" width="64">
                        </div>
                        <div class="col-sm-10">
                            <h4 class="card-diff--title">Ипотека от разных банков</h4>
                            <p class="card-diff--desc">
                                Не каждый банк одобрит ипотеку. Не каждый банк предлагает хорошую ставку и условия кредитования. Мы соберем актуальные предложения от разных банков именно для вас!
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card-diff">
                    <div class="row">
                        <div class="col-sm-2">
                            <img class="card-diff--img" src="<?= get_template_directory_uri() ?>/assets/img/notes.svg" alt="" width="64">
                        </div>
                        <div class="col-sm-10">
                            <h4 class="card-diff--title">Оформление документации </h4>
                            <p class="card-diff--desc">
                                Мы не только найдем квартиру по лучшей цене у строительной компании, но будем сопровождать вас на вех этапах «бумажной волокиты». Так быстрее. Вам понравится!
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card-diff">
                    <div class="row">
                        <div class="col-sm-2">
                            <img class="card-diff--img" src="<?= get_template_directory_uri() ?>/assets/img/stopclock.svg" alt="" width="64">
                        </div>
                        <div class="col-sm-10">
                            <h4 class="card-diff--title">Помощь до получения ключей</h4>
                            <p class="card-diff--desc">
                                Специалист – ваш персональный помощник и днем и ночью. Спрашивайте, уточняйте, планируйте без ограничения. Вы должны быть уверены в каждом квадратном метре!
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= ThemexInterface::renderSectionFree(get_field("section_free", 970)) ?>
<?= ThemexInterface::renderSectionFormRequest() ?>
<?php get_footer(); ?>