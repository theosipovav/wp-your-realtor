<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="yandex-verification" content="d7690bb327a86e6b" />
    <meta name="google-site-verification" content="KY5OrDHMyABAaRXaUAWKd2MdXqZZIv2t0__3MuKON4o" />
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <title><?= wp_get_document_title() ?></title>
    <?php wp_head(); ?>
</head>

<?php

//d();

?>

<body <?php body_class(); ?>>
    <?php
    wp_body_open();
    ?>

    <body>
        <!-- #Header -->

        <?php
        switch (get_post_type()) {
            case 'builder':
            case 'complex':
                get_template_part('header', 'post');
                break;

            default:
                get_template_part('header', 'default');
                break;
        }


        ?>