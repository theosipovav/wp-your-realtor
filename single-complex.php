<?php
/*
Template Name: Жилой комплекс
Template Post Type: complex
*/

/**
 * Подключение стилей и скриптов
 */
function enqueueScriptsComplexes()
{
    wp_enqueue_script('js-script-lightzoom', get_template_directory_uri() . "/assets/js/lightzoom.js", array('jquery'), 1.0, true);
    wp_enqueue_script('js-script-complexes', get_template_directory_uri() . "/assets/js/script-complexes.js", array('jquery', 'js-siema'), 1.0, true);
    wp_enqueue_style('lightzoom', get_template_directory_uri() . '/assets/css/lightzoom.css');
}
add_action('wp_enqueue_scripts', 'enqueueScriptsComplexes');



$args = array(
    'nopaging'               => true,
    'post_type'              => 'nav_menu_item',
    'post_status'            => 'publish',
    'order'                  => 'ASC',
    'orderby'                => 'menu_order',
    'output'                 => ARRAY_A,
    'output_key'             => 'menu_order',
    'update_post_term_cache' => false
);
$itemsMenuComplex = wp_get_nav_menu_items('Меню для ЖК', $args = array());


// 
// Фоновая картинка
$pictureBackground = get_field("picture_bg");

// 
// Наименование жилого комплекса
$nameComplex = get_the_title(get_the_ID());
if ($nameComplex == null || $nameComplex == "") {
    $nameComplex = "Наименование";
}

// 
// Ключевые особенности
$features = get_field("features");

// 
// Дополнительная картинка
$imgMore = get_field("picture_more");
if ($imgMore == null || $imgMore == "") {
    $imgMoreUrl = get_template_directory_uri() . "/assets/img/section-more-default.jpg";
    $imgMoreAlt = "";
} else {
    $imgMoreUrl = $imgMore['sizes']['large'];
    $imgMoreAlt = "";
}

// 
// 
$builderSelect = get_field("builder_select");
if ($builderSelect == null || $builderSelect == "") {
    $builderSelect = 2;
}

// 
// Планировки текущего жилого комплекса
$structures = get_field("structures");

// 
// 
$apartments = 0;

$deliveryTimes = array();



?>

<?php get_header(); ?>

<div id="HeaderPost" class="section section-header-post">
    <div class="container">
        <div class="row align-items-center in">
            <div class="col-12 col-sm-3 d-flex justify-content-center justify-content-md-start align-items-md-center">
                <a href="."><?= get_the_post_thumbnail(get_the_ID(), 'post-thumbnail', ['class' => 'logo']) ?></a>
            </div>
            <div class="col-sm-7 laptop">
                <ul class="nav">
                    <?php foreach ($itemsMenuComplex as $key => $item) : ?>
                        <li class="nav-item">
                            <a href="<?= $item->url ?>" class="scroll-to nav-link"><?= $item->title ?></a>
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>
            <div class="col-sm-2 laptop">
                <a href="#" data-modal="#ModalMain" class="btn-modal-open btn-link-primary-v2">
                    <?= ThemexInterface::renderText('Оставить заявку'); ?>

                </a>
            </div>
        </div>
    </div>
</div>
<div class="section section-banner">
    <?php if ($pictureBackground != null) : ?>
        <?= wp_get_attachment_image($pictureBackground["ID"], 'full', true, ['class' => 'img-banner-bg']) ?>
    <?php else : ?>
        <img class="img-banner-bg" src="<?= get_template_directory_uri() ?>/assets/img/section-banner-default.jpg" alt="<?= get_bloginfo("name") ?>">
    <?php endif ?>

    <div class="container">
        <div class="color-white">Жилой комплекс</div>
        <h1 class="color-white"><?= $nameComplex ?></h1>
    </div>
</div>

<div id="AboutComplex" class="section section-about my-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="py-2">О жилом комплексе</h2>
                <div class="post-content">
                    <?= wp_strip_all_tags(get_the_content()) ?>
                </div>
            </div>
        </div>
    </div>
</div>



<div id="Map" class="section my-5 p-0 background-form">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 d-flex justify-content-center align-items-center p-0" style="min-height: 390px;">
                <?= get_field("map_code") ?>
            </div>
        </div>
    </div>
</div>

<?php if (!empty($structures)) : ?>
    <div id="Apartments" class="section background-form">
        <div class="container">
            <div class="row">
                <h3><span>ЖК</span> <span><?= get_the_title() ?></span></h3>
            </div>

            <?php foreach ($structures as $key => $s) : ?>
                <?php if ($key != 0 && $key < count($structures)) : ?>
                    <hr>
                <?php endif ?>
                <?php
                $apartmentsCurrent = intval(get_field("apartments", $s->ID));
                $apartments += $apartmentsCurrent;
                $deliveryTimeCurrent =  get_field("delivery_time", $s->ID);
                if (!empty($deliveryTimeCurrent)) {
                    array_push($deliveryTimes, $deliveryTimeCurrent);
                }
                ?>
                <div class="row">
                    <div class="col-sm-7">
                        <?= ThemexInterface::renderSliderLayouts($s->ID, $key); ?>
                    </div>
                    <div class="col-sm-5 d-flex flex-column">
                        <h3 class="color-primary p-0"><?= get_field("title", $s->ID) ?></h3>
                        <p class="p-0"><?= get_field("address", $s->ID) ?></p>
                        <div class="row">
                            <div class="col-6 d-flex flex-column">
                                <?php if ($apartmentsCurrent > 0) : ?>
                                    <h3 class="color-primary p-0"><?= $apartmentsCurrent ?></h3>
                                    <p class="m-0 p-0">квартир в доме</p>
                                <?php endif ?>
                            </div>
                            <div class="col-6 d-flex flex-column">
                                <?php if (!empty($deliveryTimeCurrent["year"])) : ?>
                                    <h3 class="color-primary p-0">
                                        <?php if (!empty($deliveryTimeCurrent["quarter"])) : ?>
                                            <span><?= $deliveryTimeCurrent["quarter"] ?></span> <span>кв.</span>
                                        <?php endif ?>
                                        <span><?= $deliveryTimeCurrent["year"] ?></span> <span>г.</span>
                                    </h3>
                                    <p class="m-0 p-0">год сдачи ближайшего дома</p>
                                <?php endif ?>
                            </div>
                        </div>
                        <a href="<?= get_field("doc", $s->ID) ?>" class="btn-link color-primary d-flex align-items-center">
                            <img src="<?= get_template_directory_uri() ?>/assets/img/icon_document.svg" alt="" class="img-icon-24 me-1">
                            <span>Проектная декларация</span>
                        </a>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
<?php endif ?>

<div id="AboutBuilder" class="section">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-6 ver-line">
                <?php if ($builderSelect == 1) : ?>
                    <?php $builder = get_field("builder"); ?>
                    <?php if ($builder != null) : ?>
                        <h2 class="p-0"><a class="color-primary" href="<?= get_post_permalink($builder->ID) ?>"><?= $builder->post_title ?></a></h2>
                        <p>застройщик ЖК <?= $nameComplex ?></p>
                        <a class="color-danger py-5" href="<?= get_post_permalink($builder->ID) ?>">
                            <span>Подробнее</span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="23.524" height="12.903">
                                <path d="M17.072 0l-1.048 1.048 4.663 4.663H0v1.482h20.687l-4.663 4.663 1.048 1.048 6.452-6.452z" fill="#fa0013"></path>
                            </svg>
                        </a>
                    <?php endif ?>
                <?php endif ?>
                <?php if ($builderSelect == 2) : ?>
                    <h3 class="color-primary p-0">Застройщик ЖК <?= $nameComplex ?></h3>
                <?php endif ?>
                <?php if ($builderSelect == 3) : ?>
                    <h3 class="color-primary p-0"><?= get_field("builder_custom") ?></h3>
                <?php endif ?>
            </div>
            <div class="col-sm-1"></div>
            <div class="col-sm-5 ">
                <div class="row">
                    <div class="col-sm-6 col-6">
                        <?php if ($apartments > 0) : ?>
                            <h3 class="color-primary p-0"><?= $apartments ?></h3>
                            <span>квартир в продаже</span>
                        <?php endif ?>
                    </div>
                    <div class="col-sm-6 col-6">
                        <?php if (count($deliveryTimes) > 0) : ?>
                            <?= ThemexCore::getDateSoon($deliveryTimes); ?>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php /* ThemexInterface::renderSectionYouPrice(); */ ?>

<div id="Diff " class="section section-diff">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Бесплатные услуги</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="card-diff">
                    <div class="row">
                        <div class="col-sm-2">
                            <img class="card-diff--img" src="<?= get_template_directory_uri() ?>/assets/img/file.svg" alt="" width="64">
                        </div>
                        <div class="col-sm-10">
                            <h4 class="card-diff--title">Подбор квартиры</h4>
                            <p class="card-diff--desc">
                                Не важно, какое жилье вы ищете: однушку, трешку или пентхаус. Менеджер выслушает и составит каталог подходящих квартир У ЗАСТРОЙЩИКА. Выбирайте, смотрите, не торопитесь!
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card-diff">
                    <div class="row">
                        <div class="col-sm-2">
                            <img class="card-diff--img" src="<?= get_template_directory_uri() ?>/assets/img/skills.svg" alt="" width="64">
                        </div>
                        <div class="col-sm-10">
                            <h4 class="card-diff--title">Ипотека от разных банков</h4>
                            <p class="card-diff--desc">
                                Не каждый банк одобрит ипотеку. Не каждый банк предлагает хорошую ставку и условия кредитования. Мы соберем актуальные предложения от разных банков именно для вас!
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card-diff">
                    <div class="row">
                        <div class="col-sm-2">
                            <img class="card-diff--img" src="<?= get_template_directory_uri() ?>/assets/img/notes.svg" alt="" width="64">
                        </div>
                        <div class="col-sm-10">
                            <h4 class="card-diff--title">Оформление документации </h4>
                            <p class="card-diff--desc">
                                Мы не только найдем квартиру по лучшей цене у строительной компании, но будем сопровождать вас на вех этапах «бумажной волокиты». Так быстрее. Вам понравится!
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card-diff">
                    <div class="row">
                        <div class="col-sm-2">
                            <img class="card-diff--img" src="<?= get_template_directory_uri() ?>/assets/img/stopclock.svg" alt="" width="64">
                        </div>
                        <div class="col-sm-10">
                            <h4 class="card-diff--title">Помощь до получения ключей</h4>
                            <p class="card-diff--desc">
                                Специалист – ваш персональный помощник и днем и ночью. Спрашивайте, уточняйте, планируйте без ограничения. Вы должны быть уверены в каждом квадратном метре!
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= ThemexInterface::renderSectionFree(get_field("section_free", 970)) ?>
<?= ThemexInterface::renderSectionFormRequest() ?>

<?php get_footer(); ?>