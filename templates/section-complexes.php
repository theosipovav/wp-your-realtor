<?php


$complexes = get_posts(array(
    'numberposts' => -1,
    'category'    => 0,
    'orderby'     => 'date',
    'order'       => 'DESC',
    'include'     => array(),
    'exclude'     => array(),
    'meta_key'    => '',
    'meta_value'  => '',
    'post_type'   => 'complex',
    'suppress_filters' => true,
));
?>
<?php if (count($complexes) > 0) : ?>
    <div id="Cat" class="section">
        <div class="container">
            <h2>Каталог жилых комплексов</h2>
            <?= ThemexInterface::renderGridComplexes($complexes) ?>
        </div>
    </div>
<?php endif ?>