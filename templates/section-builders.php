<div id="ZasCat" class="section">
    <?php
    $builders = get_posts(array(
        'numberposts' => -1,
        'category'    => 0,
        'orderby'     => 'date',
        'order'       => 'DESC',
        'include'     => array(),
        'exclude'     => array(),
        'meta_key'    => '',
        'meta_value'  => '',
        'post_type'   => 'builder',
        'suppress_filters' => true,
    ));
    ?>
    <?php if (count($builders) > 0) : ?>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2>Застройщики</h2>
                </div>
                <div class="col-12">
                    <?= ThemexInterface::renderSliderBuilders($builders, 'framework/templates/grid-builders.php') ?>
                </div>
            </div>
        </div>
    <?php endif ?>
</div>