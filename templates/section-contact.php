<div id="Contact" class="section section-contact">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Контакты</h2>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-sm-5 d-flex flex-column">
                <a class="section-contact--item section-contact--phone" href="tel:<?= get_option('themex_info__tel') ?>">
                    <?= get_option('themex_info__tel') ?>
                </a>
                <label class="section-contact--label">телефон</label>
                <a class="section-contact--item section-contact--mail" href="mailto:<?= get_option('themex_info__email') ?>">
                    <?= get_option('themex_info__email') ?>
                </a>
                <label class="section-contact--label">e-mail</label>
                <div class="section-contact--item section-contact--address"><?= get_option('themex_info__address') ?></div>
                <label class="section-contact--label">адрес</label>
            </div>
            <div class="col-sm-7">
                <?= get_option('themex_info__code_map') ?>
            </div>
        </div>
    </div>
</div>