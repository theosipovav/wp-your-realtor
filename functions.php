<?php


/**
 * Отладка. Вывод информации по переменной - в.1
 */
function d($value)
{
    echo '<div class="debug-d">';
    echo "<pre>" . print_r($value, true) . "</pre>";
    echo '</div>';
}
/**
 * Отладка. Вывод информации по переменной - в.2
 */
function dd($value)
{
    echo "<pre>" . print_r($value, true) . "</pre>";
    exit();
}
/**
 * Отладка. Вывод информации по переменной - в.3
 */
function ddd($value)
{
    echo "<pre>" . htmlentities(print_r($value, true)) . "</pre>";
    exit();
}


/**
 * Включить регистрацию реколл, (когда в настройках WP она отключена)
 */
function dd3_open_rcl_register()
{
    $option = 1;
    return $option;
}
add_filter('rcl_users_can_register', 'dd3_open_rcl_register');


/**
 * Отключаем админ панель для всех, кроме администраторов.
 */
/*
if (!current_user_can('administrator')) :
    show_admin_bar(false);
endif;
*/

/** 
 * Позволяет отключить "Панель инструментов" (Админ Бар).
 * Технически функция включает/отключает "Панель" для лицевой части (фронтэнда).
 * В админ-панели "Панель" отключить невозможно.
 * 
 * Отключить: add_filter('show_admin_bar', '__return_false');
 * Включить: add_filter('show_admin_bar', '__return_true');
 */
add_filter('show_admin_bar', '__return_false');

/**
 * Включить шорткоды в текстовом редакторе
 */
add_filter('widget_text', 'do_shortcode');

/**
 * Подключение (регистрация) таблицей стилей и скриптов
 */
function enqueueScripts()
{
    wp_enqueue_style('fonts', get_template_directory_uri() . '/assets/fonts/Stem/stylesheet.css');
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/bootstrap/css/bootstrap-grid.min.css');
    wp_enqueue_style('main', get_template_directory_uri() . '/assets/css/style.min.css');
    wp_enqueue_script('js-bootstrap', get_template_directory_uri() . "/assets/bootstrap/js/bootstrap.bundle.min.js", array('jquery'), 1.0, true);
    wp_enqueue_script('js-siema', get_template_directory_uri() . "/assets/js/siema.min.js", array('jquery'), 1.0, true);
    wp_enqueue_script('js-inputmask', get_template_directory_uri() . "/assets/js/jquery.inputmask.min.js", array('jquery'), 1.0, true);
    wp_enqueue_script('js-script', get_template_directory_uri() . "/assets/js/script.js", array('jquery', 'js-siema', 'js-inputmask'), 1.0, true);
}
add_action('wp_enqueue_scripts', 'enqueueScripts');

/** 
 * Регистрирует поддержку новых возможностей темы
 * Выводит произвольное меню, созданное в панели: "внешний вид > меню" (Appearance > Menus).
 */
add_theme_support('menus');

/** 
 * Регистрирует поддержку новых возможностей темы
 * Позволяет устанавливать миниатюру посту
 */
add_theme_support('post-thumbnails');
add_theme_support('excerpt');


/** 
 * Вывод цитаты с ограничением символов
 */
function the_excerpt_max_charlength($idPost, $charlength)
{
    $res = "";
    $excerpt = get_the_excerpt($idPost);
    $charlength++;
    if (mb_strlen($excerpt) > $charlength) {
        $subex = mb_substr($excerpt, 0, $charlength - 5);
        $exwords = explode(' ', $subex);
        $excut = - (mb_strlen($exwords[count($exwords) - 1]));
        if ($excut < 0) {
            $res .= mb_substr($subex, 0, $excut);
        } else {
            $res .= $subex;
        }
        $res .= '...';
    } else {
        $res .= $excerpt;
    }
    return $res;
}

include_once 'framework/libs/Exception.php';
include_once 'framework/libs/PHPMailer.php';
include_once 'framework/libs/SMTP.php';

include_once 'framework/classes/Themex.Core.php';
include_once 'framework/classes/Themex.Interface.php';
include_once 'framework/classes/Themex.PostMetaBox.php';
include_once 'framework/classes/Themex.UpdateConsole.php';
include_once 'framework/classes/Themex.MetaBox.php';

// include_once "f.php";


define("_THEME_URL_", get_template_directory_uri());

$core = new ThemexCore();
