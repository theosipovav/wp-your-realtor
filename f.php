<?php






function true_image_uploader_field($name, $value = '', $w = 115, $h = 90)
{
    $default = get_stylesheet_directory_uri() . '/assets/img/icon-camera.svg';
    if ($value) {
        $image_attributes = wp_get_attachment_image_src($value, array($w, $h));
        $src = $image_attributes[0];
    } else {
        $src = $default;
    }

    ob_start();
    include(locate_template('framework/templates/backend/filed-add-layout.php'));
    $out = ob_get_contents();
    ob_end_clean();
    echo $out;
}




/*
 * Добавляем метабокс
 */
function true_meta_boxes_u()
{
    add_meta_box('truediv', 'Планировки квартир', 'true_print_box_u', 'layout', 'normal', 'high');
}

add_action('admin_menu', 'true_meta_boxes_u');

/*
 * Заполняем метабокс
 */
function true_print_box_u($post)
{
    if (function_exists('true_image_uploader_field')) {
        true_image_uploader_field('uploader_custom', get_post_meta($post->ID, 'uploader_custom', true));
    }
}

/*
 * Сохраняем данные произвольного поля
 */
function true_save_box_data_u($post_id)
{
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return $post_id;
    update_post_meta($post_id, 'uploader_custom', $_POST['uploader_custom']);
    return $post_id;
}

add_action('save_post', 'true_save_box_data_u');
