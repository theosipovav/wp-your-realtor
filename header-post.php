<?php


$titleSite = "Агентство недвижимости 24";
$numberPhone = get_option('themex_info__tel');
$mail = get_option('themex_info__email');
$address = get_option('themex_info__address');



$args = array(
    'nopaging'               => true,
    'post_type'              => 'nav_menu_item',
    'post_status'            => 'publish',
    'order'                  => 'ASC',
    'orderby'                => 'menu_order',
    'output'                 => ARRAY_A,
    'output_key'             => 'menu_order',
    'update_post_term_cache' => false
);
$itemsMenu = wp_get_nav_menu_items('Основное меню', $args = array());

?>
<header class="header header-post">
    <div class="container">
        <div class="row align-items-center laptop">
            <div class="col-12">
                <ul id="TopMenu" class="nav nav-top">
                    <li class="nav-item">
                        <a href="/" class="header-post--back">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-left" viewBox="0 0 16 16">
                                <path fill-rule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z" />
                            </svg>
                            <span class="ms-2">На главную</span>
                        </a>
                    </li>
                    <?php foreach ($itemsMenu as $key => $item) : ?>
                        <li class="nav-item">
                            <a href="<?= $item->url ?>" class="nav-link"><?= $item->title ?></a>
                        </li>
                    <?php endforeach ?>
                    <li class="nav-item">
                        <a href="tel:<?= $numberPhone ?>" class="nav-link"><?= $numberPhone ?></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row align-items-center mobile">
            <?= ThemexInterface::renderHeaderMenuMobile(true); ?>
        </div>
    </div>
</header>