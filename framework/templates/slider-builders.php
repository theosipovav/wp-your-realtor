<div class="slider-zascat-container">
    <div class="slider-zascat">
        <?php foreach ($items as $key => $item) : ?>
            <div class="slider-zascat-slide">
                <?= wp_get_attachment_image($item["img"]["ID"], 'medium', true, ['class' => 'slider-zascat-slide--img']) ?>
                <?= get_the_post_thumbnail($item["id"], 'post-thumbnail', ['class' => 'slider-zascat-slide--logo']) ?>
                <div class="slider-zascat-slide-body">
                    <h3 class="slider-zascat-slide--title color-white"><?= $item["title"] ?></h3>
                    <p class="slider-zascat-slide--desc laptop"><?= $item["desc"] ?></p>
                </div>
                <a href="<?= $item["url"] ?>" class="slider-zascat-slide--link"></a>
            </div>
        <?php endforeach ?>
    </div>
    <div class="slider--prev">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="white" class="bi bi-chevron-left" viewBox="0 0 16 16">
            <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" />
        </svg>
    </div>
    <div class="slider--next">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="white" class="bi bi-chevron-right" viewBox="0 0 16 16">
            <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
        </svg>
    </div>
</div>