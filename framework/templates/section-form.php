<div id="FormSelect" class="section section-form" style="background-image: url('<?= get_template_directory_uri() ?>/assets/img/section-form-03.jpg'), none;">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h2>Оставьте заявку на подбор квартиры</h2>
                <?= ThemexInterface::renderFormRequest() ?>
            </div>
            <div class="col-sm-6"></div>
        </div>
    </div>
</div>