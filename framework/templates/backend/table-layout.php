<table class="form-table table-layouts-schemes">
    <tr>
        <td colspan="2">
            <div class="table-layouts-schemes-controls">
                <button type="button" class="button button-primary button-large list-admin-layouts--add">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-circle-fill" viewBox="0 0 16 16">
                        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3v-3z" />
                    </svg>
                    <span style="margin-left: 0.5em;">Добавить схему планировки</span>
                </button>
                <button type="button" class="button button-primary button-large list-admin-layouts--add-multi">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-circle-fill" viewBox="0 0 16 16">
                        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3v-3z" />
                    </svg>
                    <span style="margin-left: 0.5em;">Сформировать автоматически из выбранных</span>
                </button>
            </div>
        </td>
    </tr>
    <?php if (count($layouts) > 0) : ?>
        <?php foreach ($layouts as $layout) : ?>
            <?= ThemexUpdateConsole::renderTableLayoutsSchemesRow($post->ID, $layout['title'], $layout['img']) ?>
        <?php endforeach ?>
    <?php else : ?>
        <?= ThemexUpdateConsole::renderTableLayoutsSchemesRow($post->ID, "", -1) ?>
    <?php endif ?>
</table>