<tr class="table-layouts-schemes-row">
    <td>
        <div class="list-admin-layouts-img">
            <img data-src="<?= $default ?>" src="<?= $src ?>" width="<?= $w ?>px" height="<?= $h ?>px" alt="<?= $alt ?>" />
            <div>
                <input class="list-admin-layouts-img--id" type="hidden" name="<?= ThemexUpdateConsole::$meta_key_img ?>[]" id="<?= $name ?>" value="<?= $idImg ?>" />
                <button type="button" class="button button-upload-image">Загрузить</button>
                <button type="button" class="button button-remove-image">×</button>
            </div>
        </div>
    </td>
    <td>
        <h4 class="h4">Заголовок для схемы</h4>
        <input class="list-admin-layouts-input" type="text" name="<?= ThemexUpdateConsole::$meta_key_title ?>[]" value="<?= esc_attr($title) ?>">
    </td>
    <td>
        <button type="button" class="button button-console button-remove" style="margin: auto;">

            <span class="dashicons dashicons-trash"></span>
            <span style="margin-left: 0.5em;">Удалить</span>
        </button>
    </td>
</tr>