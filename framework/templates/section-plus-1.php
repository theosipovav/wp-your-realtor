<!-- #Prem -->

<?php
$data = get_field("section_plus");
?>

<?php if ($data['enabled']) : ?>
    <div id="Prem" class="section section-prem">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="card-prem-2">
                        <?= ThemexInterface::renderCardPlus($data['plus_1']) ?>
                    </div>
                    <div class="card-prem-2">
                        <?= ThemexInterface::renderCardPlus($data['plus_2']) ?>
                    </div>
                </div>

                <div class="col-12 col-md-6">
                    <div class="card-prem-2">
                        <?= ThemexInterface::renderCardPlus($data['plus_3']) ?>
                    </div>
                    <div class="card-prem-2">
                        <?= ThemexInterface::renderCardPlus($data['plus_4']) ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
<?php endif ?>