<div id="Info3" class="section section-info">
    <div class="container">
        <h2 class="section-info-title color-secondary"><?= $title ?></h2>
        <div class="row">
            <?php foreach ($info as $key => $item) : ?>
                <div class="col-sm-4">
                    <div class="section-info-post flex">
                        <h4 class="section-info-post--title color-primary"><?= $item['title'] ?></h4>
                        <p class="section-info-post--desc text-pre-wrap"><?= $item['desc'] ?></p>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>