<div class="row">
    <div class="col-sm-7">
        <?= ThemexInterface::renderSliderLayouts($layouts, $key); ?>
    </div>
    <div class="col-sm-5 d-flex flex-column">
        <h3 class="color-primary p-0"><?= get_field("title", $s->ID) ?></h3>
        <p class="p-0"><?= get_field("address", $s->ID) ?></p>
        <div class="row">



            <div class="col-6 d-flex flex-column">
                <?php if ($apartmentsCurrent > 0) : ?>
                    <h3 class="color-primary p-0"><?= $apartmentsCurrent ?></h3>
                    <p class="m-0 p-0">квартир в доме</p>
                <?php endif ?>
            </div>
            <div class="col-6 d-flex flex-column">
                <h3 class="color-primary p-0"><span><?= $deliveryTimeCurrent["quarter"] ?></span> кв. <span><?= $deliveryTimeCurrent["year"] ?></span></h3>
                <p class="m-0 p-0">год сдачи ближайшего дома</p>
            </div>
        </div>
        <a href="<?= get_field("doc", $s->ID) ?>"> Проектная декларация</a>
    </div>
</div>