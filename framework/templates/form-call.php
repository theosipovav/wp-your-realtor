<?php
$isDebug = false;
$valueYourName = "";
$valueYourEmail = "";
$valueYourTel = "";
if ($isDebug) {
    $valueYourName = "Тестировщик";
    $valueYourEmail = "theosipovav.dev@yadnex.ru";
    $valueYourTel = "+71234567890";
}

?>

<form action="<?= admin_url('admin-ajax.php') ?>?action=form_send" method="post" class="form form-ajax form-call">
    <div class="form-status">
        <div class="d-flex justify-content-center align-items-center">
            <img class="form-status--img form-status--img-success" src="<?= get_template_directory_uri() ?>/assets/img/icon-success.svg" alt="">
            <img class="form-status--img form-status--img-error" src="<?= get_template_directory_uri() ?>/assets/img/icon-error.svg" alt="">
        </div>
        <p class="form-status--text"></p>
    </div>
    <div class="row">
        <div class="col-12">
            <input type="text" name="your-name" value="<?= $valueYourName ?>" size="40" class="form-control" placeholder="Как вас зовут?">
        </div>
        <div class="col-12">
            <input type="tel" name="your-tel" value="<?= $valueYourTel ?>" class="form-control form-control-tel" placeholder="+7 (XXX) XXX-XX-XX">
        </div>
        <div class="col-12">
            <input type="submit" value="Отправить заявку" class="btn btn-form mt-5" aria-invalid="false">
        </div>
    </div>
</form>