<div class="card-prem-picture-v3">
    <div class="d-flex justify-content-center align-items-center">
        <div class="w-100 m-3">
            <img src="<?= $data['img']['sizes']['medium'] ?>" class="prem--img w-100" alt="<?= $data['img']['alt'] ?>">
        </div>
    </div>

    <h3 class="card-prem--title"><?= $data['title'] ?></h3>
    <p class="card-prem--desc"><?= $data['desc'] ?></p>
</div>