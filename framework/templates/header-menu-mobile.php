<div class="col-4">
    <a href=".">
        <img class="img-fluid logo logo-header-mobile" alt="<?= $titleSite ?>" src="<?= get_template_directory_uri() ?>/assets/img/logo.png">
    </a>
</div>
<div class="col-8">
    <div class="nav-mobile-container">
        <button class="btn nav-mobile-close">
            <svg xmlns="http://www.w3.org/2000/svg" width="50.414" height="50.414" viewBox="0 0 50.414 50.414">
                <g transform="translate(-1816.793 -48.793)">
                    <line x2="49" y2="49" transform="translate(1817.5 49.5)" fill="none" stroke="#545560" stroke-width="2"></line>
                    <line x2="49" y2="49" transform="translate(1817.5 98.5) rotate(-90)" fill="none" stroke="#545560" stroke-width="2"></line>
                </g>
            </svg>
        </button>
        <ul id="HeaderMenuMobile" class="nav nav-mobile">
            <?php foreach ($itemsMenu as $key => $item) : ?>

                <li class="nav-item">
                    <a href="<?= $item->url ?>" class="scroll-to nav-link"><?= $item->title ?></a>
                </li>
            <?php endforeach ?>
            <li class="nav-item">
                <a href="#" data-modal="#ModalMain" class="nav-link nav-modal-open btn-modal-open btn-link-primary-v2">
                    <?= ThemexInterface::renderText('Оставить заявку'); ?>
                </a>
            </li>
        </ul>

        <div class="nav-mobile-more">
            <span>Звоните нам:</span>
            <a class="link-text text-bold" href="tel:<?= $numberPhone ?>"><?= $numberPhone ?></a>
            <a class="link-text color-danger" href="mailto:<?= $mail ?>"><?= $mail ?></a>
            <p><?= $address ?></p>
        </div>
    </div>
    <div class="d-flex justify-content-end">
        <button class="btn btn-nav-mobile">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-telephone" viewBox="0 0 16 16">
                <path d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z" />
            </svg>
        </button>
        <button class="btn btn-nav-mobile nav-mobile-open ms-1">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-list" viewBox="0 0 16 16">
                <path fill-rule="evenodd" d="M2.5 11.5A.5.5 0 0 1 3 11h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 7h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 3h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
            </svg>
        </button>
    </div>
</div>