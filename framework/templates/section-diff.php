<div id="Diff " class="section section-diff">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2><?= $title ?></h2>
            </div>
        </div>
        <div class="row">
            <?php foreach ($items as $key => $item) : ?>
                <div class="col-sm-6">
                    <div class="card-diff">
                        <div class="row">
                            <div class="col-3 col-sm-2">
                                <?php if ($item['is_img_default'] == 1) : ?>
                                    <img class="card-diff--img" src="<?= get_template_directory_uri() ?>/assets/img/file.svg" alt="" width="64">
                                <?php else : ?>
                                    <img class="card-diff--img" src="<?= $item['img']['url'] ?>" alt="" width="64">
                                <?php endif ?>
                            </div>
                            <div class="col-9 col-sm-10">
                                <h4 class="card-diff--title"><?= $item['title'] ?></h4>
                                <p class="card-diff--desc"><?= $item['desc'] ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>

        </div>
    </div>
</div>