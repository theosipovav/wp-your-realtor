<div class="col-12 d-flex flex-lg-column justify-content-end">
    <div class="row">
        <div class="col-sm-6 offset-sm-1 order-sm-last">
            <h1 class="main-slider-title"><?= $title ?></h1>
            <div class="row">
                <div class="col-sm-6 pr"><?= $subtitle_1 ?></div>
                <div class="col-sm-6 pr"><?= $subtitle_2 ?></div>
            </div>
        </div>
        <div class="col-sm-5 d-flex flex-column justify-content-end main-slider-intro-container">
            <div class="main-slider-intro">
                <div class="btn btn-intro">
                    <div class="btn-intro--text">
                        <div class="main-slider-intro--title"><?= $textButton["title"] ?></div>
                        <p class="m-0 p-0"><?= $textButton["subtitle"] ?></p>
                    </div>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="12">
                        <path d="M17.072 0l-1.048 1.048 4.663 4.663H0v1.482h20.687l-4.663 4.663 1.048 1.048 6.452-6.452z" fill="#fff" />
                    </svg>
                </div>
            </div>
            <a class="main-slider-phone" href="tel:<?= get_option('themex_info__tel') ?>"><?= get_option('themex_info__tel') ?></a>
            <a class="main-slider-call" href="#" data-toggle="modal" data-target="#za">Заказать звонок</a>
        </div>
    </div>
</div>