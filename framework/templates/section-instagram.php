<div class="section section-instagram">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2><?= $title ?></h2>
                <div id="Instagram" class="widget-instagram" data-username="<?= $login ?>">
                    <div class="d-flex align-items-center">
                        <div class="widget-instagram-profile flex-grow-1">
                            <div class="widget-instagram-profile-img">
                                <img src="https://scontent-arn2-2.cdninstagram.com/v/t51.2885-19/s150x150/116795435_176748050523989_4739672416220188183_n.jpg?tp=1&_nc_ht=scontent-arn2-2.cdninstagram.com&_nc_ohc=f4XVGTvrvTYAX-r_5Tl&ccb=7-4&oh=3e137e32b06ed62791d0a88c50d9030c&oe=60899EE3&_nc_sid=7bff83" alt="">
                            </div>
                            <div class="widget-instagram-profile--desc"></div>
                        </div>
                        <div class="d-flex justify-content-end align-items-center">
                            <div class="btn btn-intro">
                                <a href="https://www.instagram.com/<?= $login ?>/" class="btn-intro--text">
                                    <span class="color-white me-2">Подписаться</span>
                                </a>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="12">
                                    <path d="M17.072 0l-1.048 1.048 4.663 4.663H0v1.482h20.687l-4.663 4.663 1.048 1.048 6.452-6.452z" fill="#fff"></path>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div class="widget-instagram-feed">
                        <div class="row">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>