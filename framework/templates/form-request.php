<?php
$isDebug = false;
$valueYourName = "";
$valueYourEmail = "";
$valueYourTel = "";
if ($isDebug) {
    $valueYourName = "Тестировщик";
    $valueYourEmail = "theosipovav.dev@yadnex.ru";
    $valueYourTel = "+71234567890";
}

?>
<form action="<?= admin_url('admin-ajax.php') ?>?action=form_send" method="post" class="form form-ajax form-request">
    <div class="form-status">
        <div class="d-flex justify-content-center align-items-center">
            <img class="form-status--img form-status--img-success" src="<?= get_template_directory_uri() ?>/assets/img/icon-success.svg" alt="">
            <img class="form-status--img form-status--img-error" src="<?= get_template_directory_uri() ?>/assets/img/icon-error.svg" alt="">
        </div>
        <p class="form-status--text"></p>
    </div>
    <input type="hidden" name="form-title" value="Заявка на подбор квартиры">
    <input type="hidden" class="nonce" value="<?= wp_create_nonce(); ?>" />
    <div class="row">
        <div class="col-12">
            <input type="text" name="your-name" value="<?= $valueYourName ?>" class="form-control" placeholder="Ваше имя*" required>
        </div>
        <!--
        <div class="col-12">
            <input type="text" name="your-email" value="<?= $valueYourEmail ?>" class="form-control form-control-email" placeholder="Ваш email*" required>
        </div>
        -->
        <div class="col-12">
            <input type="text" name="your-tel" value="<?= $valueYourTel ?>" class="form-control form-control-tel" placeholder="+7 (XXX) XXX-XX-XX" required>
        </div>
        <!--
        <div class="col-12">
            <textarea name="your-message" rows="4" class="form-control" placeholder="Напишите свои пожелания — какое жилье вам нужно, в каком районе, в какой ценовой категории…"></textarea>
        </div>
        -->

    </div>
    <div class="row align-items-center pt-5">
        <div class="col-sm-6">
            <input type="submit" value="Отправить заявку" class="btn btn-form">
        </div>
        <div class="col-sm-6">
            <p class="form-more">
                Перезвоним через 15 минут и ответим на все вопросы.
            </p>
        </div>
    </div>
</form>