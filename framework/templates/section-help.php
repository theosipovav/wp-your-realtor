<div id="Info1" class="section section-info">
    <div class="container">
        <div class="row">
            <div class="col-sm-5">
                <h2 class="section-info-title color-primary"><?= $title ?></h2>
                <div class="section-info-desc"><?= $subtitle ?></div>
                <a href="#" class="section-info-link color-primary-v2">
                    <?= ThemexInterface::renderText('Купить квартиру безопасно'); ?>
                </a>
            </div>
            <div class="col-sm-6 offset-sm-1">
                <div class="section-info-post">
                    <h4 class="section-info-post--title"><?= $plus['plus_1']['title'] ?></h4>
                    <p class="section-info-post--desc"><?= $plus['plus_1']['desc'] ?></p>
                </div>
                <div class="section-info-post">
                    <h4 class="section-info-post--title"><?= $plus['plus_2']['title'] ?></h4>
                    <p class="section-info-post--desc"><?= $plus['plus_2']['desc'] ?></p>
                </div>
                <div class="section-info-post">
                    <h4 class="section-info-post--title"><?= $plus['plus_3']['title'] ?></h4>
                    <p class="section-info-post--desc"><?= $plus['plus_3']['desc'] ?></p>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <img alt="Наша команда" class="img-info-line" src="<?= $src ?>">

            </div>
        </div>
    </div>
</div>