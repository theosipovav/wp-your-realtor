<div class="mx-3" style="position: relative;">
    <?php if (count($schemes)) : ?>
        <div class="slider-structures slider-structures-<?= $key ?>">
            <?php foreach ($schemes as $s) : ?>
                <div class="slider-structures-slide">
                    <div class="d-flex justify-content-center align-items-center flex-grow-1">
                        <a href="<?= $s['img']["full"][0] ?>" class="lightzoom d-flex">
                            <?= wp_get_attachment_image($s['img']["id"], 'medium', true, ['class' => 'slider-structures--img']) ?>
                        </a>
                    </div>
                    <h4 class="slider-structures--title"><?= $s["title"] ?></h4>
                </div>
            <?php endforeach ?>
        </div>
        <div class="slider--prev slider-structures--prev-<?= $key ?>">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="white" class="bi bi-chevron-left" viewBox="0 0 16 16">
                <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" />
            </svg>
        </div>
        <div class="slider--next slider-structures--next-<?= $key ?>">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="white" class="bi bi-chevron-right" viewBox="0 0 16 16">
                <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
            </svg>
        </div>
    <?php endif ?>
</div>