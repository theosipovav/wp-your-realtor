<div id="Info4" class="section section-info">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2><?= $title ?></h2>
            </div>
            <div class="col-12">
                <div class="tab">
                    <ul class="tab-nav">
                        <?php foreach ($items as $key => $item) : ?>
                            <li class="tab-nav-item">
                                <a class="tab-nav-link <?php if ($key == 0) echo "active"; ?>" data-toggle="tab" href="#s<?= $key ?>"><?= $item['btn'] ?></a>
                            </li>
                        <?php endforeach ?>
                    </ul>
                    <div class="tab-content">
                        <?php foreach ($items as $key => $item) : ?>
                            <?php
                            $itemsListString = ThemexCore::explodeList($item['list_string']);
                            ?>
                            <?php if (count($itemsListString) > 0) : ?>
                                <div class="tab-pane fade <?php if ($key == 0) echo "show active"; ?>" id="s<?= $key ?>">
                                    <div class="row align-items-center">
                                        <div class="col-12 col-sm-7">
                                            <div class="section-info-post--title"><?= $item['title'] ?></div>
                                            <div class="section-info-post--desc"><?= $item['desc'] ?></div>
                                        </div>
                                        <div class="col-12 col-sm-5">
                                            <ul class="list">
                                                <?php foreach ($itemsListString as $str) : ?>
                                                    <li class="list-item"><?= $str ?></li>
                                                <?php endforeach ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            <?php else : ?>
                                <div class="tab-pane fade <?php if ($key == 0) echo "show active"; ?>" id="s<?= $key ?>">
                                    <div class="row align-items-center">
                                        <div class="col-12">
                                            <div class="section-info-post--title"><?= $item['title'] ?></div>
                                            <div class="section-info-post--desc"><?= $item['desc'] ?></div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif ?>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>