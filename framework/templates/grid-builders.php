<div class="row">
    <?php foreach ($items as $key => $item) : ?>
        <div class="col-6">
            <div class="slider-zascat-slide mb-3">
                <?= wp_get_attachment_image($item["img"]["ID"], 'medium', true, ['class' => 'slider-zascat-slide--img']) ?>
                <?= get_the_post_thumbnail($item["id"], 'post-thumbnail', ['class' => 'slider-zascat-slide--logo']) ?>
                <div class="slider-zascat-slide-body">
                    <h3 class="slider-zascat-slide--title color-white"><?= $item["title"] ?></h3>
                    <p class="slider-zascat-slide--desc laptop"><?= $item["desc"] ?></p>
                </div>
                <a href="<?= $item["url"] ?>" class="slider-zascat-slide--link"></a>
            </div>
        </div>
    <?php endforeach ?>
</div>