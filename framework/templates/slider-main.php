<div class="slider-main">
    <div class="slider-main-container">
        <?php foreach ($slides as $key => $s) : ?>
            <div class="slider-main-slide">
                <img src="<?= $s['url'] ?>" class="slider-main--cover" alt="">
            </div>
        <?php endforeach ?>

    </div>
    <div class="slider-main-text-overflow">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="main-slider-title color-white "><?= $title ?></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-4 d-flex justify-content-start align-items-center pr color-white pt-3">
                    <?= $subtitle_1 ?>
                </div>
                <div class="col-12 col-sm-4 d-flex justify-content-start align-items-center pr color-white pt-3">
                    <?= $subtitle_2 ?>
                </div>
                <div class="col-12 col-sm-4 d-flex justify-content-start align-items-center pr color-white pt-3">
                    <div class="d-flex flex-column">
                        <a class="main-slider-phone color-white" href="tel:<?= get_option('themex_info__tel') ?>"><?= get_option('themex_info__tel') ?></a>
                        <a class="btn-modal-open main-slider-call color-white" href="#" data-toggle="modal" data-modal="#ModalMain">Заказать звонок</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>