<div class="section section-separation" style="background-image: url(<?= $src ?>);">
    <div class="row">
        <div class="col-12">
            <h3 class="display-1 text-center color-white p-0"><?= $title ?></h3>
        </div>
    </div>
</div>