<?php
$groupSlides = [];
$count = 0;
$groupCount = 0;
$groupSlides[$groupCount] = [];
foreach ($items as $key => $item) {
    if ($count > 7) {
        $count = 0;
        $groupCount++;
        $groupSlides[$groupCount] = [];
    }
    $count++;
    $groupSlides[$groupCount][] = $item;
}
?>
<div class="row">
    <div class="col-12">
        <div class="slider-complexes-container">
            <div class="slider-complexes">
                <?php foreach ($groupSlides as $groupSlide) : ?>
                    <div class="slider-complexes-slide">
                        <div class="row">
                            <?php foreach ($groupSlide as $id) : ?>
                                <div class="card-cat col-sm-3 col-6 p-0">
                                    <a href="<?= get_post_permalink($id) ?>" class="card-cat--link-stretched">
                                        <?= get_the_post_thumbnail($id, 'post-thumbnail', ['class' => 'card-cat--img']) ?>
                                    </a>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
            <div class="slider--prev slider-complexes--prev">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="white" class="bi bi-chevron-left" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" />
                </svg>
            </div>
            <div class="slider--next slider-complexes--next">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="white" class="bi bi-chevron-right" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
                </svg>
            </div>
        </div>
    </div>
</div>