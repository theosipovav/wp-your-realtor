<div id="Prem" class="section section-prem">
    <div class="container">
        <div class="row">
            <?php foreach ($items as $key => $item) : ?>
                <div class="col-12 col-md-3">
                    <?= ThemexInterface::renderCardPlusV3($item) ?>
                </div>
            <?php endforeach ?>

        </div>
    </div>
</div>