<div id="Free" class="section section-free">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 section-free-stand">
                <h2 class="section-free--title"><?= $title ?></h2>
                <p class="section-free--desc color-primary-v2 m-0 p-0">
                    <strong>Какие подводные камни? – Их просто нет</strong>
                </p>
                <p class="section-free--desc m-0 p-0">
                    <strong>Тогда как это работает?</strong>
                </p>
                <ol class="free-list">
                    <?php
                    foreach ($list as $key => $item) : ?>
                        <li class="free-list--item"><?= $item ?></li>
                    <?php endforeach ?>
                </ol>

            </div>
            <div class="col-sm-6 section-free-post bg-primary">
                <?= $desc ?>
            </div>
        </div>
    </div>
</div>