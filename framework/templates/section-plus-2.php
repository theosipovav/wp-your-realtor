<!-- #Prem -->
<div id="Prem" class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-6">
                <div class="card-prem">
                    <img alt="Актуальная база" class="card-prem--img" src="https://24pnz.ru/wp-content/themes/24pnz/images/p1.jpg" srcset="https://24pnz.ru/wp-content/themes/24pnz/images/p1.jpg 1x, https://24pnz.ru/wp-content/themes/24pnz/images/p1_2x.jpg 2x">
                    <h3 class="card-prem--title">Актуальная база</h3>
                    <p class="card-prem--desc">
                        квартир от Рисан, ГСЗ, РКС, Территории жизни, Термодома и других застройщиков Пензы
                    </p>
                </div>

            </div>
            <div class="col-md-3 col-6">
                <div class="card-prem">
                    <img alt="Цена недвижимости" class="card-prem--img" src="https://24pnz.ru/wp-content/themes/24pnz/images/p2.jpg" srcset="https://24pnz.ru/wp-content/themes/24pnz/images/p2.jpg 1x, https://24pnz.ru/wp-content/themes/24pnz/images/p2_2x.jpg 2x">
                    <h3 class="card-prem--title">Цена недвижимости</h3>
                    <p class="card-prem--desc">
                        на сайте = официальная цена в офисе строительной компании
                    </p>
                </div>
            </div>
            <div class="col-md-3 col-6">
                <div class="card-prem">
                    <img alt="Подбор квартиры" class="card-prem--img" src="https://24pnz.ru/wp-content/themes/24pnz/images/p3.jpg" srcset="https://24pnz.ru/wp-content/themes/24pnz/images/p3.jpg 1x, https://24pnz.ru/wp-content/themes/24pnz/images/p3_2x.jpg 2x">
                    <h3 class="card-prem--title">Подбор квартиры</h3>
                    <p class="card-prem--desc">
                        в том районе, где удобнее вам! Без комиссии и прочих оплат
                    </p>
                </div>
            </div>
            <div class="col-md-3 col-6">
                <div class="card-prem">
                    <img alt="Гарантии" class="card-prem--img" src="https://24pnz.ru/wp-content/themes/24pnz/images/p4.jpg" srcset="https://24pnz.ru/wp-content/themes/24pnz/images/p4.jpg 1x, https://24pnz.ru/wp-content/themes/24pnz/images/p4_2x.jpg 2x">
                    <h3 class="card-prem--title">Гарантии</h3>
                    <p class="card-prem--desc">
                        если мы не выполним обязательства, выплатим 50 000 рублей!
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>