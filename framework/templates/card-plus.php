<div class="card-prem-picture">
    <?php if ($data['is_img_default'] && !is_array($data['img'])) : ?>
        <img src="<?= get_template_directory_uri() ?>/assets/img/actual_base.svg" class="prem--img" alt="">
    <?php else : ?>
        <img src="<?= $data['img']['sizes']['medium'] ?>" class="prem--img" alt="<?= $data['img']['alt'] ?>">
    <?php endif ?>
</div>
<div class="d-flex flex-column">
    <h3 class="card-prem--title mt-0"><?= $data['title'] ?></h3>
    <p class="card-prem--desc"><?= $data['desc'] ?></p>
</div>