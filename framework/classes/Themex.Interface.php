<?php


class ThemexInterface
{
    function __construct()
    {
        //
    }



    public static function renderHeaderMenuMobile($isLinkToHome = false)
    {

        $args = array(
            'nopaging'               => true,
            'post_type'              => 'nav_menu_item',
            'post_status'            => 'publish',
            'order'                  => 'ASC',
            'orderby'                => 'menu_order',
            'output'                 => ARRAY_A,
            'output_key'             => 'menu_order',
            'update_post_term_cache' => false
        );
        $itemsMenu = wp_get_nav_menu_items('Основное меню', $args = array());
        $numberPhone = get_option('themex_info__tel');
        $mail = get_option('themex_info__email');
        $address = get_option('themex_info__address');

        ob_start();
        include(locate_template('framework/templates/header-menu-mobile.php'));
        $out = ob_get_contents();
        ob_end_clean();
        return $out;
    }



    public static function renderSliderMain($data)
    {
        $title = $data['title'];
        $subtitle_1 = $data['subtitle_1'];
        $subtitle_2 = $data['subtitle_2'];

        $textButton = array();
        $textButton["title"] = "Выгодная ипотека от 4,5%";
        $textButton["subtitle"] = "Найдем самую выгодную процентную ставку<br>по ипотеке среди всех банков.";


        $slides = array();
        foreach ($data['slider'] as $key => $s) {
            if (!empty($s)) {
                array_push($slides, $s);
            }
        }

        if (count($slides) == 0) {
            array_push($slides, _THEME_URL_ . '/assets/img/slider-main-01.jpg');
        }

        ob_start();
        include(locate_template('framework/templates/slider-main.php'));
        $out = ob_get_contents();
        ob_end_clean();
        return $out;
    }



    /**
     * Слайдер "Застройщики"
     */
    public static function renderSliderBuilders($builders, $pathTemplate = 'framework/templates/slider-builders.php')
    {
        $items = array();
        foreach ($builders as $key => $builder) {
            $item = array();
            $item["img"] = get_field("picture_bg", $builder->ID);
            $item["id"] = $builder->ID;
            $item["title"] = $builder->post_title;
            $item["desc"] = the_excerpt_max_charlength($builder->ID, 150);
            $item["url"] = get_post_permalink($builder->ID);
            array_push($items, $item);
        }
        $res = "";
        if (count($items) > 0) {
            ob_start();
            include(locate_template($pathTemplate));
            $res = ob_get_contents();
            ob_end_clean();
        }
        return $res;
    }



    /**
     * Сетка "Каталог жилых комплексов"
     */
    public static function renderGridComplexes($complexes)
    {
        $items = array();
        foreach ($complexes as $key => $complex) {
            array_push($items, $complex->ID);
        }
        $res = "";
        if (count($items) > 0) {
            ob_start();
            include(locate_template('framework/templates/grid-complexes.php'));
            $res = ob_get_contents();
            ob_end_clean();
        }
        return $res;
    }


    /**
     * Сетка "Строения"
     */
    public static function renderDatagridStructures($structures)
    {
        $res = "";
        if (count($structures) > 0) {
            foreach ($structures as $key => $s) {
                $apartmentsCurrent = intval(get_field("apartments", $s->ID));
                $deliveryTimeCurrent =  get_field("delivery_time", $s->ID);
                //$apartments += $apartmentsCurrent;
                $layouts = get_posts(array(
                    'numberposts' => -1,
                    'orderby'     => 'date',
                    'order'       => 'DESC',
                    'post_type'   => 'layout',
                    'post_parent' => $s->ID,
                    'suppress_filters' => true,
                ));
                ob_start();
                include(locate_template('framework/templates/datagrid-structures.php'));
                $out = ob_get_contents();
                ob_end_clean();
                $res .= $out;
            }
        }
        return $res;
    }





    public static function renderSectionPlus($data)
    {
        if (!$data['enabled']) {
            return '';
        }

        $items = array();
        if (!empty($data['plus_1'])) array_push($items, $data['plus_1']);
        if (!empty($data['plus_2'])) array_push($items, $data['plus_2']);
        if (!empty($data['plus_3'])) array_push($items, $data['plus_3']);
        if (!empty($data['plus_4'])) array_push($items, $data['plus_4']);

        ob_start();
        include(locate_template('framework/templates/section-plus-3.php'));
        $out = ob_get_contents();
        ob_end_clean();
        return $out;
    }

    public static function renderCardPlus($data)
    {

        ob_start();
        include(locate_template('framework/templates/card-plus.php'));
        $out = ob_get_contents();
        ob_end_clean();
        return $out;
    }
    public static function renderCardPlusV3($data)
    {
        ob_start();
        include(locate_template('framework/templates/card-plus-3.php'));
        $out = ob_get_contents();
        ob_end_clean();
        return $out;
    }








    /**
     * 
     */
    public static function renderSliderLayouts($idStructure, $key)
    {

        $arrayTitle = get_post_meta($idStructure, ThemexUpdateConsole::$meta_key_title, true);
        $arrayImg = get_post_meta($idStructure, ThemexUpdateConsole::$meta_key_img, true);

        $schemes = array();
        for ($i = 0; $i < count($arrayImg); $i++) {

            $id = intval($arrayImg[$i]);

            $s = array();
            $s["title"] = $arrayTitle[$i];
            $s['img'] = array();
            $s['img']["id"] = "";
            $s['img']["thumbnail"] =  "";
            $s['img']["medium"] =  "";
            $s['img']["large"] =  "";
            $s['img']["full"] =  "";

            if (!empty($id) && $id != -1 && $id != 0) {
                $s['img']["id"] = $id;
                $s['img']["alt"] = get_post_meta($id, '_wp_attachment_image_alt', true);
                $s['img']["thumbnail"] =  wp_get_attachment_image_src($id, 'thumbnail', false);
                $s['img']["medium"] =  wp_get_attachment_image_src($id, 'medium', false);
                $s['img']["large"] =  wp_get_attachment_image_src($id, 'large', false);
                $s['img']["full"] =  wp_get_attachment_image_src($id, 'full', false);
            }
            array_push($schemes,  $s);
        }


        ob_start();
        include(locate_template('framework/templates/slider-layouts.php'));
        $out = ob_get_contents();
        ob_end_clean();
        return $out;
    }





    public static function renderSectionSeparation($data)
    {
        if (!$data['enabled']) {
            return '';
        }
        if (!$data['is_img_default'] && is_array($data['img'])) {
            $src = $data['img']['url'];
        }
        $title = $data['title'];
        ob_start();
        include(locate_template('framework/templates/section-separation.php'));
        $out = ob_get_contents();
        ob_end_clean();
        return $out;
    }



    public static function renderSectionHelp($data)
    {
        if (!$data['enabled']) {
            return '';
        }
        $title = $data['title'];
        $subtitle = $data['subtitle'];
        $plus = $data['plus'];
        $src = get_template_directory_uri() . '/assets/img/our_team.jpg';
        if (!$data['is_img_default'] && is_array($data['img'])) {
            $src = $data['img']['url'];
        }
        ob_start();
        include(locate_template('framework/templates/section-help.php'));
        $out = ob_get_contents();
        ob_end_clean();
        return $out;
    }

    public static function renderSectionAbout($data)
    {
        if (!$data['enabled']) {
            return '';
        }
        $title = $data['title'];
        $info = array();
        array_push($info, $data['info_1']);
        array_push($info, $data['info_2']);
        array_push($info, $data['info_3']);

        ob_start();
        include(locate_template('framework/templates/section-about.php'));
        $out = ob_get_contents();
        ob_end_clean();
        return $out;
    }

    public static function renderSectionServices($data)
    {
        if (!$data['enabled']) {
            return '';
        }
        $title = $data['title'];
        $list = array();
        foreach ($data['list'] as $item) {
            array_push($list, $item);
        }
        $desc = $data['desc'];


        ob_start();
        include(locate_template('framework/templates/section-services.php'));
        $out = ob_get_contents();
        ob_end_clean();
        return $out;
    }


    public static function renderSectionFree($data)
    {
        if (!$data['enabled']) {
            return '';
        }
        $title = $data['title'];
        $items = array();

        if (!empty($data['item_1']) && !empty($data['item_1']['enabled']) && $data['item_1']['enabled'] == 1) {
            if ($data['item_1']['enabled'] == 1) array_push($items, $data['item_1']);
        }
        if (!empty($data['item_2']) && !empty($data['item_2']['enabled']) && $data['item_2']['enabled'] == 1) {
            if ($data['item_2']['enabled'] == 1) array_push($items, $data['item_2']);
        }
        if (!empty($data['item_3']) && !empty($data['item_3']['enabled']) && $data['item_3']['enabled'] == 1) {
            if ($data['item_3']['enabled'] == 1) array_push($items, $data['item_3']);
        }
        if (!empty($data['item_4']) && !empty($data['item_4']['enabled']) && $data['item_4']['enabled'] == 1) {
            if ($data['item_4']['enabled'] == 1) array_push($items, $data['item_4']);
        }
        if (!empty($data['item_5']) && !empty($data['item_5']['enabled']) && $data['item_5']['enabled'] == 1) {
            if ($data['item_5']['enabled'] == 1) array_push($items, $data['item_5']);
        }
        ob_start();
        include(locate_template('framework/templates/section-free.php'));
        $out = ob_get_contents();
        ob_end_clean();
        return $out;
    }


    public static function renderSectionYouPrice($pathTemplate = "section-you-price.php")
    {
        ob_start();
        include(locate_template('framework/templates/' . $pathTemplate));
        $out = ob_get_contents();
        ob_end_clean();
        return $out;
    }

    public static function renderSectionDiff($data)
    {
        if (!$data['enabled']) {
            return '';
        }
        $title = $data['title'];
        $items = array();
        array_push($items, $data['item_1']);
        array_push($items, $data['item_2']);
        array_push($items, $data['item_3']);
        array_push($items, $data['item_4']);
        ob_start();
        include(locate_template('framework/templates/section-diff.php'));
        $out = ob_get_contents();
        ob_end_clean();
        return $out;
    }

    public static function renderSectionInstagram($data)
    {
        if (!$data['enabled']) {
            return '';
        }
        $title = $data['title'];
        $login = $data['login'];
        ob_start();
        include(locate_template('framework/templates/section-instagram.php'));
        $out = ob_get_contents();
        ob_end_clean();
        return $out;
    }



    public static function renderSectionFormRequest()
    {
        ob_start();
        include(locate_template('framework/templates/section-form.php'));
        $out = ob_get_contents();
        ob_end_clean();
        return $out;
    }


    public static function renderFormRequest()
    {
        ob_start();
        include(locate_template('framework/templates/form-request.php'));
        $out = ob_get_contents();
        ob_end_clean();
        return $out;
    }
    public static function renderFormCall()
    {
        ob_start();
        include(locate_template('framework/templates/form-call.php'));
        $out = ob_get_contents();
        ob_end_clean();
        return $out;
    }




    public static function renderText($text)
    {
        $htmlString = "";
        if ($text == 'Оставить заявку') {
            $htmlString = '<span class="me-2">Оставить заявку</span>';
            $htmlString .= '<svg xmlns="http://www.w3.org/2000/svg" width="23.524" height="12.903"><path d="M17.072 0l-1.048 1.048 4.663 4.663H0v1.482h20.687l-4.663 4.663 1.048 1.048 6.452-6.452z" fill="currentcolor"></path></svg>';
        }
        if ($text == 'Купить квартиру безопасно') {
            $htmlString = '<span>Купить квартиру безопасно</span>';
            $htmlString .= '<svg xmlns="http://www.w3.org/2000/svg" width="23.524" height="12.903"><path d="M17.072 0l-1.048 1.048 4.663 4.663H0v1.482h20.687l-4.663 4.663 1.048 1.048 6.452-6.452z" fill="currentcolor"></path></svg>';
        }

        return $htmlString;
    }
}
