<?php



if (!class_exists('ThemexCore')) :



    /**
     * 
     */

    class ThemexCore
    {

        public $mail;

        public function __construct()
        {
            $updateConsole = new ThemexUpdateConsole();
            add_action('wp_ajax_widget_message_check', 'ajaxWidgetMessageCheck');
            add_action('wp_ajax_form_send', array('ThemexCore', 'ajaxFormSend'));
            add_action('wp_ajax_nopriv_form_send', array('ThemexCore', 'ajaxFormSend'));
        }





        /**
         * Получить год сдачи ближайшего дома
         */
        static function getDateSoon($dates)
        {
            $dateSoon = array();
            $dateSoon['year'] = "";
            $dateSoon['quarter'] = "";
            if (count($dates) == 0) {
                return '<h4 class="color-primary p-0">Все дома сданы</h4>';
            }
            $dateCurrent = new DateTime();
            $yearCurrent = intval($dateCurrent->format('Y'));
            $monthCurrent = intval($dateCurrent->format('m'));
            $quarterCurrent = self::getCurrentQuarter($monthCurrent);
            foreach ($dates as $date) {
                if ($date['year'] < $yearCurrent) {
                    continue;
                }
                if ($date['year'] == $yearCurrent && $date['quarter'] < $quarterCurrent) {
                    continue;
                }
                if ($dateSoon['year'] == "" || $dateSoon['quarter'] == "") {
                    $dateSoon['year'] = $date["year"];
                    $dateSoon['quarter'] = $date["quarter"];
                    continue;
                }
                $diffSoon = ($dateSoon['year'] - $yearCurrent) * 10 + ($dateSoon['quarter'] - $quarterCurrent);
                $diffCurrent = ($date['year'] - $yearCurrent) * 10 + ($date['quarter'] - $quarterCurrent);
                if ($diffCurrent < $diffSoon) {
                    $dateSoon['year'] = $date['year'];
                    $dateSoon['quarter'] = $date['quarter'];
                }
            }
            return '<h3 class="color-primary p-0"><span>' . $dateSoon['quarter'] . '</span> кв. <span>' . $dateSoon['year'] . '</span></h3><span>год сдачи ближайшего дома</span>';
        }

        /**
         * Получить текущий квартал по месяцу
         */
        static function getCurrentQuarter($month)
        {
            if ($month >= 1 && $month <= 3) {
                return 1;
            }
            if ($month >= 4 && $month <= 6) {
                return 2;
            }
            if ($month >= 7 && $month <= 9) {
                return 3;
            }
            if ($month >= 10 && $month <= 12) {
                return 4;
            }
        }


        static function ajaxFormSend()
        {
            $data = $_POST;
            $from = "info@maximum-rielty.ru";
            $to = get_option('themex_info__email');


            if (isset($data["form-title"])) {
                $subject = $data["form-title"];
            } else {
                $subject = "Сообщение от формы обратной связи";
            }
            $message = "Поступило новое сообщение.<br />";
            if (isset($data["your-name"])) {
                $message .= "Имя: " . htmlspecialchars($data['your-name']) . "<br />";
            }
            if (isset($data["your-email"])) {
                $message .= "Email: " . htmlspecialchars($data['your-email']) . "<br />";
            }

            if (isset($data["your-tel"])) {
                $message .= "Телефон: " . htmlspecialchars($data['your-tel']) . "<br />";
            }
            if (isset($data["your-message"])) {
                $message .= "Сообщение:<br />" . htmlspecialchars($data['your-message']) . "<br />";
            }
            $head = "MIME-Version: 1.0\r\n";
            $head .= "Content-type: text/html; charset=utf-8\r\n";
            $head .= "From: " . $from . "\r\n";
            $head .= "Reply-To: " . $from . "E\r\n";
            $result = array();
            $result["status"] = "error";
            $result["msg"] = "Обработка отправки сообщения окончилась с ошибкой";
            if (mail($to, $subject, $message, $head)) {
                $result["status"] = "success";
                $result["msg"] = $data;
                echo json_encode($result);
            } else {
                $result["status"] = "error";
                $result["msg"] = "Не удалось отправить сообщение";
                echo json_encode($result);
            }
            die;
        }


        static public function explodeList($string, $d = ';')
        {
            $array = array();
            foreach (explode($d, $string) as $key => $item) {
                if (empty($item)) continue;
                $item = trim($item, " \t\n\r\0\x0B");
                array_push($array, $item);
            }
            return $array;
        }
    }









endif;
