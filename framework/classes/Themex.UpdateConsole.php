<?php

if (!class_exists('ThemexUpdateConsole')) :
    /**
     * 
     */
    class ThemexUpdateConsole
    {

        public static $post_type = 'layout';
        public static $meta_key_title = 'layout_schema_title';
        public static $meta_key_img = 'layout_schema_img';


        public function __construct()
        {
            // Подключение стилей и скриптов для консоли
            add_action('admin_enqueue_scripts', array($this, 'loadScriptForConsole'));

            //  Регистрация нового типа записи "builder" - "Застройщик"
            add_action('init', array('ThemexUpdateConsole', 'initPostBuilder'));

            //  Регистрация нового типа записи "complex" - "Жилой комплекс"
            add_action('init', array('ThemexUpdateConsole', 'initPostComplex'));

            // Регистрация нового типа записи "layout" - "Планировки квартир"
            add_action('init', array('ThemexUpdateConsole', 'initPostLayout'));

            // Инициализация дополнительных полей в раздел настройки темы
            add_action('customize_register', array('ThemexUpdateConsole', 'customizeRegisterSettingTheme'));




            // Создать группа произвольных полей "Планировки квартир"
            add_action('add_meta_boxes', array($this, 'addMetaBoxLayoutApartment'));


            add_action('save_post', array($this, 'saveMetaBox'));
        }

        /**
         * Подключение стилей и скриптов для консоли
         * 
         * В админке уже должен быть подключен jQuery, если нет: wp_enqueue_script('jquery');
         */
        function loadScriptForConsole()
        {
            // дальше у нас идут скрипты и стили загрузчика изображений WordPress
            if (!did_action('wp_enqueue_media')) {
                wp_enqueue_media();
            }
            wp_enqueue_style('style-console', get_template_directory_uri() . '/assets/css/style-console.min.css');
            wp_enqueue_script('script-console', get_stylesheet_directory_uri() . '/assets/js/script-console.js', array('jquery'), null, false);
        }


        /** 
         * Регистрация нового типа записи "builder" - "Застройщик"
         */
        static function initPostBuilder()
        {
            register_post_type('builder', array(
                'labels'             => array(
                    'name'               => 'Каталог застройщиков',
                    'singular_name'      => 'Застройщик',
                    'add_new'            => 'Добавить застройщика',
                    'add_new_item'       => 'Добавить застройщика',
                    'edit_item'          => 'Редактировать',
                    'new_item'           => 'Новый застройщик',
                    'view_item'          => 'Посмотреть',
                    'search_items'       => 'Найти',
                    'not_found'          => 'Не найдено',
                    'parent_item_colon'  => '',
                    'not_found_in_trash' => 'В корзине записей не найдено',
                    'menu_name'          => 'Застройщики'

                ),
                'public'             => true,
                'publicly_queryable' => true,
                'show_ui'            => true,
                'show_in_menu'       => true,
                'query_var'          => true,
                'rewrite'            => true,
                'capability_type'    => 'post',
                'has_archive'        => true,
                'hierarchical'       => false,
                'menu_position'      => null,
                'menu_icon'          => 'dashicons-businessman',
                'supports'           => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'page-attributes')
            ));
        }

        /** 
         * Регистрация нового типа записи "complex" - "Жилой комплекс"
         */
        static function initPostComplex()
        {
            register_post_type('complex', array(
                'labels'             => array(
                    'name'               => 'Каталог жилых комплексов',
                    'singular_name'      => 'Жилой комплекс',
                    'add_new'            => 'Добавить комплекс',
                    'add_new_item'       => 'Добавить комплекс',
                    'edit_item'          => 'Редактировать',
                    'new_item'           => 'Новый жилой комплекс',
                    'view_item'          => 'Посмотреть',
                    'search_items'       => 'Найти',
                    'not_found'          => 'Не найдено',
                    'parent_item_colon'  => '',
                    'not_found_in_trash' => 'В корзине записей не найдено',
                    'menu_name'          => 'Жилые комплексы'

                ),
                'public'             => true,
                'publicly_queryable' => true,
                'show_ui'            => true,
                'show_in_menu'       => true,
                'query_var'          => true,
                'rewrite'            => true,
                'capability_type'    => 'post',
                'has_archive'        => true,
                'hierarchical'       => false,
                'menu_position'      => null,
                'menu_icon'          => 'dashicons-building',
                'supports'           => array('title', 'editor', 'thumbnail', 'page-attributes')
            ));
        }







        /** 
         * Регистрация нового типа записи "layout" - "Планировки квартир"
         */
        static function initPostLayout()
        {
            register_post_type('layout', array(
                'labels'             => array(
                    'name'               => 'Каталог планировок',
                    'singular_name'      => 'Планировка квартиры',
                    'add_new'            => 'Добавить планировку',
                    'add_new_item'       => 'Добавить планировку',
                    'edit_item'          => 'Редактировать',
                    'new_item'           => 'Новая планировка',
                    'view_item'          => 'Посмотреть',
                    'search_items'       => 'Найти',
                    'not_found'          => 'Не найдено',
                    'parent_item_colon'  => '',
                    'not_found_in_trash' => 'В корзине записей не найдено',
                    'menu_name'          => 'Планировки квартир'

                ),
                'public'             => true,
                'publicly_queryable' => true,
                'show_ui'            => true,
                'show_in_menu'       => true,
                'query_var'          => true,
                'rewrite'            => true,
                'capability_type'    => 'post',
                'has_archive'        => true,
                'hierarchical'       => true,
                'menu_position'      => null,
                'menu_icon'          => 'dashicons-building',
                'supports'           => array('title', 'page-attributes'),
                'taxonomies' => array('type_layout'),
            ));
        }








        /** 
         * Инициализация дополнительных полей в раздел настройки темы
         */
        static function customizeRegisterSettingTheme($wp_customize)
        {
            // Секция Контакты
            $wp_customize->add_section(
                'themex_info',
                array(
                    'title' => 'Информация',
                    'priority' => 30,
                    //'panel' => 'themex_panel_footer_customization',
                )
            );
            //
            // Телефон для связи
            $wp_customize->add_setting(
                'themex_info__tel',
                array(
                    'default' => '+7 (123) 4567-78-90',
                    'type' => 'option'
                )
            );
            $wp_customize->add_control(
                'themex_info__tel_control',
                array(
                    'label' => 'Телефон',
                    'type' => 'text',
                    'section' => 'themex_info',
                    'settings' => 'themex_info__tel',
                    'description' => 'пример: +7 (123) 1234 56 78',
                )
            );
            //
            // Почта для связи
            $wp_customize->add_setting(
                'themex_info__email',
                array(
                    'default' => 'info@example.com',
                    'type' => 'option'
                )
            );
            $wp_customize->add_control(
                'themex_info__email_control',
                array(
                    'label' => 'Почта',
                    'type' => 'email',
                    'section' => 'themex_info',
                    'settings' => 'themex_info__email',
                    'description' => 'пример: info@example.com',
                )
            );
            //
            // Адрес
            $wp_customize->add_setting(
                'themex_info__address',
                array(
                    'default' => '',
                    'type' => 'option'
                )
            );
            $wp_customize->add_control(
                'themex_info__address_control',
                array(
                    'label' => 'Адрес',
                    'type' => 'text',
                    'section' => 'themex_info',
                    'settings' => 'themex_info__address',
                    'description' => 'пример: Пресненская наб., 12, Москва, 123317',
                )
            );
            //
            // Код карты
            $wp_customize->add_setting(
                'themex_info__code_map',
                array(
                    'default' => '',
                    'type' => 'option'
                )
            );
            $wp_customize->add_control(
                'themex_info__code_map_control',
                array(
                    'label' => 'JS код карты',
                    'type' => 'textarea',
                    'section' => 'themex_info',
                    'settings' => 'themex_info__code_map',
                )
            );
            //
            // Часы работы
            $wp_customize->add_setting(
                'themex_info__timework',
                array(
                    'default' => 'ПН, ВТ, СР, ЧТ, ПТ, СБ: с 9:00 до 18:00, ВС: Выходной',
                    'type' => 'option'
                )
            );
            $wp_customize->add_control(
                'themex_info__timework_control',
                array(
                    'label' => 'Время работы',
                    'type' => 'text',
                    'section' => 'themex_info',
                    'settings' => 'themex_info__timework',
                    'description' => 'пример: ПН, ВТ, СР, ЧТ, ПТ, СБ: с 9:00 до 18:00, ВС: Выходной',
                )
            );




            // Ссылка на аккаунт в Vk
            $wp_customize->add_setting(
                'themex_info__vk',
                array(
                    'default' => '',
                    'type' => 'option'
                )
            );
            $wp_customize->add_control(
                'themex_info__vk_control',
                array(
                    'label' => 'Ссылка на аккаунт в Vk',
                    'type' => 'text',
                    'section' => 'themex_info',
                    'settings' => 'themex_info__vk',
                    'description' => 'при пустом значение ссылка на страницу не отобразится',
                )
            );
            // Ссылка на Instagram
            $wp_customize->add_setting(
                'themex_info__instagram',
                array(
                    'default' => '',
                    'type' => 'option'
                )
            );
            $wp_customize->add_control(
                'themex_info__instagram_control',
                array(
                    'label' => 'Ссылка на Instagram аккаунт',
                    'type' => 'text',
                    'section' => 'themex_info',
                    'settings' => 'themex_info__instagram',
                    'description' => 'при пустом значение ссылка на страницу не отобразится',
                )
            );
            // Ссылка на аккаунт в Facebook
            $wp_customize->add_setting(
                'themex_info__facebook',
                array(
                    'default' => '',
                    'type' => 'option'
                )
            );
            $wp_customize->add_control(
                'themex_info__facebook_control',
                array(
                    'label' => 'Ссылка на аккаунт в Facebook',
                    'type' => 'text',
                    'section' => 'themex_info',
                    'settings' => 'themex_info__facebook',
                    'description' => 'при пустом значение ссылка на страницу не отобразится',
                )
            );
            // Ссылка на аккаунт в 2Gis
            $wp_customize->add_setting(
                'themex_info__2gis',
                array(
                    'default' => '',
                    'type' => 'option'
                )
            );
            $wp_customize->add_control(
                'themex_info__2gis_control',
                array(
                    'label' => 'Ссылка на аккаунт в 2Gis',
                    'type' => 'text',
                    'section' => 'themex_info',
                    'settings' => 'themex_info__2gis',
                    'description' => 'при пустом значение ссылка на страницу не отобразится',
                )
            );
            // Ссылка на аккаунт в Whatsapp
            $wp_customize->add_setting(
                'themex_info__whatsapp',
                array(
                    'default' => '',
                    'type' => 'option'
                )
            );
            $wp_customize->add_control(
                'themex_info__whatsapp_control',
                array(
                    'label' => 'Ссылка на аккаунт в Whatsapp',
                    'type' => 'text',
                    'section' => 'themex_info',
                    'settings' => 'themex_info__whatsapp',
                    'description' => 'при пустом значение ссылка на страницу не отобразится',
                )
            );
        }






        //
        //
        //

        /**
         * Создать группа произвольных полей "Планировки квартир"
         */
        public function addMetaBoxLayoutApartment()
        {
            add_meta_box('metabox_layout_apartment', 'Планировки квартир', array($this, 'renderTableLayout'), self::$post_type, 'advanced', 'high');
        }


        /**
         * Отрисовать интерфейс добавления и удаления планировок
         */
        public function renderTableLayout($post)
        {
            $titles = get_post_meta($post->ID, self::$meta_key_title, true);
            $images = get_post_meta($post->ID, self::$meta_key_img, true);



            $layouts = array();
            if (!empty($images)) {
                for ($i = 0; $i < count($images); $i++) {
                    $layout = array();
                    $layout['title'] = $titles[$i];
                    $layout['img'] = $images[$i];
                    array_push($layouts, $layout);
                }
            }


            ob_start();
            include(locate_template('framework/templates/backend/table-layout.php'));
            $out = ob_get_contents();
            ob_end_clean();
            echo $out;
        }

        /**
         * Отрисовать строку таблицы
         */
        static function renderTableLayoutsSchemesRow($idPost, $title, $idImg)
        {
            $default = get_stylesheet_directory_uri() . '/assets/img/icon-camera.svg';
            $w = 75;
            $h = 75;
            if ($idImg && $idImg != -1) {
                $image_attributes = wp_get_attachment_image_src($idImg, array($w, $h));
                $alt = get_post_meta($idImg, '_wp_attachment_image_alt', true);

                $src = $image_attributes[0];
            } else {
                $src = $default;
            }
            ob_start();
            include(locate_template('framework/templates/backend/table-layouts-schemes-row.php'));
            $out = ob_get_contents();
            ob_end_clean();
            echo $out;
        }

        /**
         * Очищает и сохраняет значения полей
         */
        function saveMetaBox($post_id)
        {
            //
            if (wp_is_post_autosave($post_id)) {
                return;
            }

            //
            if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
                return $post_id;
            }




            if (isset($_POST[self::$meta_key_title]) && is_array($_POST[self::$meta_key_title])) {
                $items = $_POST[self::$meta_key_title];

                // Очистить
                $items = array_map('sanitize_text_field', $items);

                // Убрать пустые поля?
                $isFilter = false;
                if ($isFilter) {
                    $items = array_filter($items);
                }


                if ($items) {
                    update_post_meta($post_id, self::$meta_key_title, $items);
                } else {
                    delete_post_meta($post_id, self::$meta_key_title);
                }
            }


            if (isset($_POST[self::$meta_key_img]) && is_array($_POST[self::$meta_key_img])) {
                $items = $_POST[self::$meta_key_img];

                // Очистить
                $items = array_map('sanitize_text_field', $items);

                // Убрать пустые поля?
                $isFilter = false;
                if ($isFilter) {
                    $items = array_filter($items);
                }

                if ($items) {
                    update_post_meta($post_id, self::$meta_key_img, $items);
                } else {
                    delete_post_meta($post_id, self::$meta_key_img);
                }
            }

            return $post_id;
        }
    }
endif;
