<!-- footer -->
<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since Twenty Seventeen 1.0
 * @version 1.2
 */

$titleSite = get_bloginfo("name");
$address = get_option('themex_info__address');
$email = get_option('themex_info__email');
$phone = get_option('themex_info__tel');

$linkVk = get_option('themex_info__vk');
$linkInstagram = get_option('themex_info__instagram');
$linkTwitter = get_option('themex_info__twitter');
$linkFacebook = get_option('themex_info__facebook');
$linkYoutube = get_option('themex_info__youtube');
$link2gis = get_option('themex_info__2gis');
$linkWhatsapp = get_option('themex_info__whatsapp');


$args = array(
    'nopaging'               => true,
    'post_type'              => 'nav_menu_item',
    'post_status'            => 'publish',
    'order'                  => 'ASC',
    'orderby'                => 'menu_order',
    'output'                 => ARRAY_A,
    'output_key'             => 'menu_order',
    'update_post_term_cache' => false
);
$itemsMenu = wp_get_nav_menu_items('Основное меню', $args = array());
?>
<!-- #Footer -->
<div id="Footer" class="footer">
    <div class="container">

        <div class="row count">


            <!-- Контакты -->
            <div class="col-sm-3 d-flex flex-column">
                <a href="tel:<?= $phone ?>" class="footer--item p-0"><?= $phone ?></a>
                <a href="mailto:info@24pnz.ru" class="footer--item"><?= $email ?></a>
                <div class="footer--item color-light"><?= $address ?></div>
            </div>

            <!-- Меню -->
            <div class="col-sm-2">
                <div class="footer-menu-container">
                    <ul class="footer-menu">
                        <?php for ($i = 0; $i < 3; $i++) : ?>
                            <li class="footer-menu-item">
                                <a href="<?= $itemsMenu[$i]->url ?>" class="scroll-to footer-menu--link" aria-current="page"><?= $itemsMenu[$i]->title ?></a>
                            </li>
                        <?php endfor ?>
                    </ul>
                </div>
            </div>

            <!-- Логотип -->
            <div class="col-sm-2 d-flex justify-content-center">
                <a href="/">
                    <img class="img-fluid logo logo-footer" alt="РИЭЛТОР21" src="<?= get_template_directory_uri() ?>/assets/img/logo-black.png">
                </a>
            </div>

            <!-- Меню -->
            <div class="col-sm-2">
                <div class="footer-menu-container">
                    <ul class="footer-menu">
                        <?php for ($i = 3; $i < 6; $i++) : ?>
                            <li class="footer-menu-item">
                                <a href="<?= $itemsMenu[$i]->url ?>" class="scroll-to footer-menu--link" aria-current="page"><?= $itemsMenu[$i]->title ?></a>
                            </li>
                        <?php endfor ?>
                    </ul>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="footer-social">
                    <?php if (!empty($linkVk)) : ?>
                        <a href="<?= $linkVk ?>" class="footer-social-item">
                            <img alt="Мы во Вконтакте" src="<?= get_template_directory_uri() ?>/assets/img/icon_vk.svg" class="footer-social--img">
                        </a>
                    <?php endif ?>
                    <?php if (!empty($linkInstagram)) : ?>
                        <a href="<?= $linkInstagram ?>" class="footer-social-item">
                            <img alt="Мы в Instagram" src="<?= get_template_directory_uri() ?>/assets/img/icon_instagram.svg" class="footer-social--img">
                        </a>
                    <?php endif ?>
                    <?php if (!empty($linkFacebook)) : ?>
                        <a href="<?= $linkFacebook ?>" class="footer-social-item">
                            <img alt="Мы в Facebook" src="<?= get_template_directory_uri() ?>/assets/img/icon_facebook.svg" class="footer-social--img">
                        </a>
                    <?php endif ?>
                    <?php if (!empty($link2gis)) : ?>
                        <a href="<?= $link2gis ?>" class="footer-social-item">
                            <img alt="Мы в 2Gis" src="<?= get_template_directory_uri() ?>/assets/img/icon_2gis.svg" class="footer-social--img">
                        </a>
                    <?php endif ?>
                    <?php if (!empty($linkWhatsapp)) : ?>
                        <a href="<?= $linkWhatsapp ?>" class="footer-social-item">
                            <img alt="Мы в Whatsapp" src="<?= get_template_directory_uri() ?>/assets/img/icon_whatsapp.svg" class="footer-social--img">
                        </a>
                    <?php endif ?>
                </div>
            </div>


        </div>


        <div class="row footer-info">
            <div class="col-sm-4">
                <span class="footer--item color-light">© 2021 «<?= $titleSite ?>»</span>
            </div>
            <div class="col-sm-4">
                <a class="footer--item color-light" href="/politika-konfidentsialnosti/">
                    Политика конфиденциальности
                </a>
            </div>
            <div class="col-sm-1"></div>
            <div class="col-sm-3 d-flex"></div>
        </div>
    </div>
</div>
<!-- #ModalMain -->
<div class="modal fade" id="ModalMain" tabindex="-1" role="dialog" aria-modal="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="btn modal-close btn-modal-close" data-modal="#ModalMain" data-dismiss="modal" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="50.414" height="50.414" viewBox="0 0 50.414 50.414">
                        <g transform="translate(-1816.793 -48.793)">
                            <line x2="49" y2="49" transform="translate(1817.5 49.5)" fill="none" stroke="#545560" stroke-width="2"></line>
                            <line x2="49" y2="49" transform="translate(1817.5 98.5) rotate(-90)" fill="none" stroke="#545560" stroke-width="2"></line>
                        </g>
                    </svg>
                </button>
                <div class="d-flex h-100 align-items-center">
                    <div class="info">
                        <div class="modal-title ">Оставьте свои контакты.</div>
                        <div class="modal-desc">Мы перезвоним в течение 15 минут и ответим на все, интересующие вас, вопросы.</div>
                        <div role="form form-modal" class="wpcf7" id="wpcf7-f386-o1" lang="ru-RU" dir="ltr">
                            <div class="screen-reader-response">
                                <p role="status" aria-live="polite" aria-atomic="true"></p>
                                <ul></ul>
                            </div>
                            <?= ThemexInterface::renderFormCall() ?>
                        </div>
                        <div class="modal-info">Нажимая на кнопку «Отправить заявку», вы даёте согласие на обработку своих персональных данных</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="FloatingCallback" class="floating floating-call fade">
    <button class="btn btn-float btn-modal-open" data-modal="#ModalMain">
        <div class="floating-icon">
            <svg xmlns="http://www.w3.org/2000/svg" width="64" height="64" fill="currentColor" class="bi bi-telephone-fill" viewBox="0 0 16 16">
                <path fill-rule="evenodd" d="M1.885.511a1.745 1.745 0 0 1 2.61.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z" />
            </svg>
        </div>
        <div class="floating-text">
            Оставить заявку
        </div>
    </button>
</div>


<?php if (!empty($linkWhatsapp)) : ?>
    <div id="FloatingWhatsapp" class="floating floating-whatsapp fade">
        <a href="<?= $linkWhatsapp ?>" target="_blank" class="btn btn-float">
            <div class="floating-icon">
                <img alt="Написать в Whatsapp" src="<?= get_template_directory_uri() ?>/assets/img/icon_whatsapp_color.svg" class="">
            </div>
        </a>
    </div>
<?php endif ?>



<?php wp_footer(); ?>
</body>

</html>