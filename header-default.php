<?php


$titleSite = "Агентство недвижимости 24";
$numberPhone = get_option('themex_info__tel');
$mail = get_option('themex_info__email');
$address = get_option('themex_info__address');



$args = array(
    'nopaging'               => true,
    'post_type'              => 'nav_menu_item',
    'post_status'            => 'publish',
    'order'                  => 'ASC',
    'orderby'                => 'menu_order',
    'output'                 => ARRAY_A,
    'output_key'             => 'menu_order',
    'update_post_term_cache' => false
);
$itemsMenu = wp_get_nav_menu_items('Основное меню', $args = array());
?>

<header id="Header" class="header header-fixed">
    <div class="h-100 d-flex flex-column">
        <div class="container-fluid">
            <div class="row row-address">
                Агентство недвижимости «Максимум» на Володарского 94а
            </div>
        </div>
        <div class="container">
            <div class="row align-items-center laptop h-100">
                <div class="col-sm-12">
                    <ul id="HeaderMenu" class="nav nav-header">
                        <?php foreach ($itemsMenu as $key => $item) : ?>
                            <?php if ($key == 3) : ?>
                                <li class="nav-item nav-item-logo h-100">
                                    <a href="/">
                                        <img class="img-fluid logo logo-header" alt="<?= $titleSite ?>" src="<?= get_template_directory_uri() ?>/assets/img/logo.png">
                                    </a>
                                </li>
                            <?php endif ?>
                            <li class="nav-item">
                                <a href="<?= $item->url ?>" class="scroll-to nav-link"><?= $item->title ?></a>
                            </li>
                        <?php endforeach ?>
                        <li class="nav-item">
                            <a href="#" data-modal="#ModalMain" class="btn-modal-open btn-link-primary-v2">
                                <?= ThemexInterface::renderText('Оставить заявку'); ?>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row align-items-center mobile h-100">
                <?= ThemexInterface::renderHeaderMenuMobile(); ?>
            </div>
        </div>
    </div>
</header>